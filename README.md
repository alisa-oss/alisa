# Changelog

##### Version 11.14 (2021-09-22)
- update alisa.net.Http and Https - fix bug on no close

##### Version 11.13 (2021-07-25)
- maven plugin for generate md5 and sha1 files
- pass SonarQube scan

##### Version 11.12 (2021-05-09)
- remove NTPClient
- remove DateTime.updateSystemTime
- remove gnu.io.* (RXTX)

##### Version 11.10 (2021-04-07)
- DateTime 
  - Fix critical bug on findPrevious(from, year, month, day) - minus 1 on Month
  - toPreciseString()
  - toPreciseTimeString()
  - add this._calendar.clear(); in constructor - for setting millisec = 0
- Add SSH

##### Version 11.6 (2021-03-17)
- DateTime
  - parseUTC()
  - change parsing format from yyyy-MM-dd kk:mm:ss to yyyy-MM-dd HH:mm:ss

##### Version 11.5 (2021-02-14)
- remove Class.forName("com.mysql.jdbc.Driver"); to sql.MySQL - deprecated

##### Version 11.4 (2020-12-05)
- add alisa.net.Http send()
- add alisa.net.Https send()

##### Version 11.3 (2020-11-19)
- add sql.SQLServer
- add Class.forName("com.mysql.jdbc.Driver"); to sql.MySQL
- add Calculator - signedToUnsigned(), unsignedToSigned()
- fixed bux on SQL.select(..., order) - this._sql += " ORDER BY " + order; // from where

##### Version 11.2 (2020-10-14)
- add QRCode
- add Random

##### Version 11.1 (2020-09-18)
- add try-catch to json.Parser on loadObject() and loadArray()

##### Version 11.0.1 (2020-06-26)
- alisa.net.Http and alisa.net.Https

##### Version 11.0 (2020-05-21)
- start
- fix critical bug on SQL (that cause memory leak)

----

# Inspirations

##### security.CheckSum
https://www.mkyong.com/java/how-to-generate-a-file-checksum-value-in-java/
