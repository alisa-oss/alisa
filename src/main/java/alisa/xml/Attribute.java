package alisa.xml;

public class Attribute 
{
    public String name;
    public String text;

    public Attribute(String name, String text)
    {
    	this.name = name;
    	this.text = text;
    }
}
