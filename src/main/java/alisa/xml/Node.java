package alisa.xml;

import java.util.ArrayList;

public class Node {

	public String name = "";
	public String text = "";

	private ArrayList<Node> _nodes = new ArrayList<Node>();
	private ArrayList<Attribute> _attributes = new ArrayList<Attribute>();
	private ArrayList<Comment> _comments = new ArrayList<Comment>();

	public Node(String name) {
		this.name = name;
	}

	// ------------ Nodes ---------------
	public void addNode(Node node) {
		this._nodes.add(node);
	}

	public Node getNode(int index) {
		return this._nodes.get(index);
	}

	public int countNodes() {
		return this._nodes.size();
	}

	public Node findNode(String name) {
		// return null if not found
		for (int i = 0; i < this._nodes.size(); i++) {
			Node node = this._nodes.get(i);
			if (node.name.equals(name)) {
				return node;
			}
		}
		return null;
	}

	public Node findNode(String name, int start) {
		// return null if not found
		for (int i = start; i < this._nodes.size(); i++) {
			Node node = this._nodes.get(i);
			if (node.name.equals(name)) {
				return node;
			}
		}
		return null;
	}

	// ------------ Attributes -------------
	public void addAttribute(Attribute attr) {
		this._attributes.add(attr);
	}

	public Attribute getAttribute(int index) {
		return this._attributes.get(index);
	}

	public int countAttributes() {
		return this._attributes.size();
	}

	public Attribute findAttribute(String name) {
		for (int i = 0; i < this._attributes.size(); i++) {
			Attribute attribute = this._attributes.get(i);
			if (attribute.name.equals(name)) {
				return attribute;
			}
		}
		return null;
	}

	public Attribute findAttribute(String name, int start) {
		for (int i = start; i < this._attributes.size(); i++) {
			Attribute attribute = this._attributes.get(i);
			if (attribute.name.equals(name)) {
				return attribute;
			}
		}
		return null;
	}

	// ------------ Comments ---------------
	public void addComment(Comment comment) {
		this._comments.add(comment);
	}

	public Comment getComment(int index) {
		return this._comments.get(index);
	}

	public int countComments() {
		return this._comments.size();
	}

}
