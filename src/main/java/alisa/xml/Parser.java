package alisa.xml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Parser {

	public Parser() {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			this._dBuilder = dbFactory.newDocumentBuilder();
		}
		catch (Exception ex) {
		}
	}

	public alisa.xml.Node load(String pathname) {
		File file = new File(pathname);

		if (this._dBuilder == null) {
			this._error = "DocumentBuilder is null.";
			return null;
		}

		try {
			Document doc = this._dBuilder.parse(file);

			// get root element
			NodeList list = doc.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node xml_node = list.item(i);
				if (xml_node.getNodeType() == Node.ELEMENT_NODE) {
					return this._getNode(xml_node);
				}
			}
		}
		catch (Exception ex) {
			this._error = ex.getMessage();
		}

		return null;
	}

	public alisa.xml.Node read(String xml) {
		if (this._dBuilder == null) {
			this._error = "DocumentBuilder is null.";
			return null;
		}

		InputStream is = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));

		try {
			Document doc = this._dBuilder.parse(is);

			// get root element
			NodeList list = doc.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node xml_node = list.item(i);
				if (xml_node.getNodeType() == Node.ELEMENT_NODE) {
					return this._getNode(xml_node);
				}
			}
		}
		catch (Exception ex) {
			this._error = ex.getMessage();
		}

		return null;
	}

	public String write(alisa.xml.Node node) {
		String xml = this._writeXml(node, 0);
		return xml;
	}

	public boolean save(String pathname, alisa.xml.Node node) {
		File file = new File(pathname);

		FileOutputStream output = null;
		try {
			output = new FileOutputStream(file);
			output.write(this.write(node).getBytes());
			output.close();
		}
		catch (Exception ex) {
		}
		finally {
			if (output != null) {
				try { output.close(); } catch (IOException ex) { }
			}
		}

		return true;
	}

	// ----------------- internal functions ------------------
	private alisa.xml.Node _getNode(Node xml_node) {
		// new node
		alisa.xml.Node node = new alisa.xml.Node(xml_node.getNodeName());

		// get attributes
		NamedNodeMap map = xml_node.getAttributes();
		for (int i = 0; i < map.getLength(); i++) {
			Node attr_node = map.item(i);
			Attribute attr = new Attribute(attr_node.getNodeName(), attr_node.getNodeValue());
			node.addAttribute(attr);
		}

		// get children
		NodeList list = xml_node.getChildNodes();
		int comment_index = 0;
		for (int i = 0; i < list.getLength(); i++) {
			Node child_xml_node = list.item(i);
			if (child_xml_node.getNodeType() == Node.ELEMENT_NODE) {
				alisa.xml.Node child_node = this._getNode(child_xml_node);
				node.addNode(child_node);
				comment_index++;
			}
			else if (child_xml_node.getNodeType() == Node.COMMENT_NODE) {
				alisa.xml.Comment child_comment = new alisa.xml.Comment(comment_index, child_xml_node.getTextContent());
				node.addComment(child_comment);
			}
		}

		if (node.countNodes() == 0) {
			node.text = xml_node.getTextContent();
		}

		return node;
	}

	private String _writeXml(alisa.xml.Node node, int tab) {
		String xml = "";

		// <NodeName
		for (int i = 0; i < tab; i++) {
			xml += "\t"; // tab
		}
		xml += "<" + node.name;

		// <NodeName attribute="string" ...
		for (int i = 0; i < node.countAttributes(); i++) {
			Attribute attribute = node.getAttribute(i);
			xml += " " + attribute.name + "=\"" + attribute.text + "\"";
		}

		if (node.countNodes() == 0) {
			if (node.text.length() == 0) {
				// <NodeName attribute="string" />
				xml += " />\r\n";
			}
			else {
				// <NodeName attribute="string">string</NodeName>
				xml += ">" + node.text + "</" + node.name + ">\r\n";
			}
		}
		else {
			int comment_index = 0;

			// <NodeName attribute="string">
			//      <NodeName...
			//      ...
			// </NodeName>
			xml += ">\r\n";
			for (int i = 0; i < node.countNodes(); i++) {
				// write comments
				for (int ii = comment_index; ii < node.countComments(); ii++) {
					Comment comment = node.getComment(comment_index);
					if (comment.getNodeIndex() == i) {
						for (int j = 0; j < tab + 1; j++) {
							xml += "\t"; // tab
						}
						xml += "<!-- " + comment.getText() + " -->\r\n";
						comment_index++;
					}
					else {
						break;
					}
				}

				// write child node
				xml += this._writeXml(node.getNode(i), tab + 1);
			}

			for (int i = 0; i < tab; i++) {
				xml += "\t"; // tab
			}
			xml += "</" + node.name + ">\r\n";
		}

		return xml;
	}

	// ------------ Properties ---------------
	public String getError() {
		return this._error;
	}
	private String _error = "";

	private DocumentBuilder _dBuilder = null;
}
