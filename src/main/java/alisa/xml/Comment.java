package alisa.xml;

public class Comment 
{
    public String getText() { return this._text; }
    private String _text = "";

    public int getNodeIndex() { return this._node_index; }
    private int _node_index = -1;

    public Comment(int node_index, String text) 
    {
        this._node_index = node_index;
        this._text = text;
    }
}
