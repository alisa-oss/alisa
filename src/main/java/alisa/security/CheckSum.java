package alisa.security;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CheckSum {   

    public static String SHA256(String file_path) {

        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA, MD2, MD5, SHA-256, SHA-384...
            
            try (InputStream fis = new FileInputStream(file_path)) {
                byte[] buffer = new byte[1024];
                int nread;
                while ((nread = fis.read(buffer)) != -1) {
                    md.update(buffer, 0, nread);
                }
            }

            // bytes to hex
            StringBuilder result = new StringBuilder();
            for (byte b : md.digest()) {
                result.append(String.format("%02x", b));
            }
            return result.toString();
        }
        catch (IOException | NoSuchAlgorithmException ex) { }
        
         return "";
    }
    public static String SHA256(byte[] buffer) {

        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256"); //SHA, MD2, MD5, SHA-256, SHA-384...
            
            md.update(buffer);

            // bytes to hex
            StringBuilder result = new StringBuilder();
            for (byte b : md.digest()) {
                result.append(String.format("%02x", b));
            }
            return result.toString();
        }
        catch (NoSuchAlgorithmException ex) {  }
        
        return "";
    }
    
    public static String SHA1(String file_path) {

        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1"); //SHA, MD2, MD5, SHA-256, SHA-384...
            
            try (InputStream fis = new FileInputStream(file_path)) {
                byte[] buffer = new byte[1024];
                int nread;
                while ((nread = fis.read(buffer)) != -1) {
                    md.update(buffer, 0, nread);
                }
            }

            // bytes to hex
            StringBuilder result = new StringBuilder();
            for (byte b : md.digest()) {
                result.append(String.format("%02x", b));
            }
            return result.toString();
        }
        catch (IOException | NoSuchAlgorithmException ex) { }
        
         return "";
    }
    public static String SHA1(byte[] buffer) {

        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1"); //SHA, MD2, MD5, SHA-256, SHA-384...
            
            md.update(buffer);

            // bytes to hex
            StringBuilder result = new StringBuilder();
            for (byte b : md.digest()) {
                result.append(String.format("%02x", b));
            }
            return result.toString();
        }
        catch (NoSuchAlgorithmException ex) {  }
        
        return "";
    }
}
