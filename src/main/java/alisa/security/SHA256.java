package alisa.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256 
{
    private MessageDigest _md = null;
    
    public boolean init()
    {
        try 
        {
            this._md = MessageDigest.getInstance("SHA-256");
        }
        catch (NoSuchAlgorithmException ex) { return false; }
        return true;        
    }
    
    public byte[] hash(byte[] raw)
    {
        return this._md.digest(raw);
    }
}
