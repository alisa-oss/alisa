package alisa.security;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES128 {

	public static byte[] generateKey(String password) {
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			return Arrays.copyOfRange(md.digest(password.getBytes()), 0, 16); // truncate to 128 bit
		}
		catch (NoSuchAlgorithmException ex) { }
		
		return null;
	}

//	public static byte[] generateIV() {
//		SecureRandom random = new SecureRandom();
//		byte[] iv = new byte[16];
//		random.nextBytes(iv);
//		return iv;
//	}

	public static byte[] encrypt(byte[] plain, byte[] key) {
		
		byte[] iv = new byte[16];
		random.nextBytes(iv);
		
		try {
			SecretKeySpec secret_key = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret_key, new IvParameterSpec(iv));
			return cipher.doFinal(plain);
		}
		catch (InvalidAlgorithmParameterException | InvalidKeyException
				  | NoSuchAlgorithmException | BadPaddingException
				  | IllegalBlockSizeException | NoSuchPaddingException ex) {
		}

		return null;
	}

	public static byte[] decrypt(byte[] secret, byte[] key) {
		
		byte[] iv = new byte[16];
		random.nextBytes(iv);

		try {
			SecretKeySpec secret_key = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secret_key, new IvParameterSpec(iv));
			return cipher.doFinal(secret);

		}
		catch (InvalidAlgorithmParameterException | InvalidKeyException
				  | NoSuchAlgorithmException | BadPaddingException
				  | IllegalBlockSizeException | NoSuchPaddingException ex) {
		}

		return null;
	}

	private final static SecureRandom random = new SecureRandom();
}
