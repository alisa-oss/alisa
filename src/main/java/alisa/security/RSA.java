package alisa.security;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSA {
	
	// key pair
	public static KeyPair generateKeyPair() throws NoSuchAlgorithmException {
		
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		keyGen.initialize(2048);
		return keyGen.generateKeyPair();
	}

	// private key
	public static PrivateKey getPrivateKey(KeyPair keyPair) {
		
		return keyPair.getPrivate();
	}

	public static byte[] storePrivateKeyToByteArray(PrivateKey private_key) { 
		
		return private_key.getEncoded();
	}

	public static PrivateKey restorePrivateKeyFromByteArray(byte[] private_bytes) 
			  throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(private_bytes));
	}

	public static String storePrivateKeyToBase64(PrivateKey private_key) {
		
		byte[] private_bytes = private_key.getEncoded();
		return new String(alisa.Base64.encode(private_bytes));
	}

	public static PrivateKey restorePrivateKeyFromBase64(String private_base64) 
			  throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		byte[] private_bytes = alisa.Base64.decode(private_base64);
		return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(private_bytes));
	}

	// public key  
	public static PublicKey getPublicKey(KeyPair keyPair) {
		
		return keyPair.getPublic();
	}

	public static byte[] storePublicKeyToByteArray(PublicKey public_key) {
		
		return public_key.getEncoded();
	}

	public static PublicKey restorePublicKeyFromByteArray(byte[] public_bytes) 
			  throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(public_bytes));
	}

	public static String storePublicKeyToBase64(PublicKey public_key) {
		
		byte[] public_bytes = public_key.getEncoded();
		return new String(alisa.Base64.encode(public_bytes));
	}

	public static PublicKey restorePublicKeyFromBase64(String public_base64) 
			  throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		byte[] public_bytes = alisa.Base64.decode(public_base64);
		return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(public_bytes));
	}

	// cipher
	public static Cipher createEncryptCipher(PublicKey public_key) 
			  throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		Cipher cipher = Cipher.getInstance("RSA/None/OAEPWITHSHA-256ANDMGF1PADDING");
		//Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, public_key);
		return cipher;
	}

	public static Cipher createDecryptCipher(PrivateKey private_key) 
			  throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		Cipher cipher = Cipher.getInstance("RSA/None/OAEPWITHSHA-256ANDMGF1PADDING");
		//Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, private_key);
		return cipher;
	}

	// encrypt
	public static String encrypt(Cipher cipher, String text) throws IllegalBlockSizeException, BadPaddingException {
		
		byte[] encrypted = cipher.doFinal(text.getBytes());
		return alisa.Base64.encode(encrypted);
	}

	public static byte[] encrypt(Cipher cipher, byte[] data) throws IllegalBlockSizeException, BadPaddingException {
		
		return cipher.doFinal(data);
	}

	// decrypt
	public static String decrypt(Cipher cipher, String base64) throws IllegalBlockSizeException, BadPaddingException {
		
		byte[] dec = alisa.Base64.decode(base64);
		return new String(cipher.doFinal(dec));
	}

	public static byte[] decrypt(Cipher cipher, byte[] data) throws IllegalBlockSizeException, BadPaddingException {
		
		return cipher.doFinal(data);
	}
}
