package alisa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CSV {

	public String getError() {
		
		return this._error;
	}
	private String _error = "";

	public String[][] parse(String url) {
		
		ArrayList<String> lines = new ArrayList<>();
		BufferedReader in = null;
		try {
			File fileDirs = new File(url);
			in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDirs), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				lines.add(line);
			}
		} 
		catch (IOException ex) { this._error = url + " " + ex.getMessage(); }
		
		if (in != null) { try { in.close(); } catch (IOException ex) { } }
		else { return null; }

		String[][] csv = new String[lines.size()][];

		for (int y = 0; y < csv.length; y++) {
			ArrayList<String> cols = new ArrayList<>();
			String s = "";
			boolean word = false;
			String line = lines.get(y);
			for (int i = 0; i < line.length(); i++) {
				char c = line.charAt(i);
				if (c == '"') // word
				{
					word = !word;
				} else if (c == ',') // new column
				{
					if (!word) {
						cols.add(s);
						s = "";
					} else {
						s += c;
					}
				} else {
					s += c;
				}
			}
			cols.add(s);

			csv[y] = new String[cols.size()];
			for (int i = 0; i < csv[y].length; i++) {
				csv[y][i] = cols.get(i);
			}
		}

		return csv;
	}
}
