package alisa.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDP {

	public static int Timeout = 5000;
	public static int BufferSize = 1024;

	public void open(String address, int port) 
			  throws SocketException, UnknownHostException {
		
		this._socket = new DatagramSocket();
		this._socket.setSoTimeout(this._timeout);
		this._address = address;
		this._inet_address = InetAddress.getByName(this._address);
		this._port = port;
	}

	public void open(String address, int port, int timeout, int buffer_size) 
			  throws SocketException, UnknownHostException {
		
		this._timeout = timeout;
		this._received_buffer = new byte[buffer_size];
		this.open(address, port);
	}

	public void close() {
		
		if (this._socket != null) { this._socket.close(); this._socket = null; }
	}

	public void send(byte[] data) throws IOException {
		
		DatagramPacket sendPacket = new DatagramPacket(data, data.length, this._inet_address, this._port);
		this._socket.send(sendPacket);
	}

	public int receive() throws IOException {
		
		DatagramPacket receivePacket = new DatagramPacket(this._received_buffer, this._received_buffer.length);
		this._socket.receive(receivePacket);
		return receivePacket.getLength();
	}

	public byte[] receiveBytes() throws IOException {
		
		DatagramPacket receivePacket = new DatagramPacket(this._received_buffer, this._received_buffer.length);
		this._socket.receive(receivePacket);
		int length = receivePacket.getLength();
		if (length > 0) {
			byte[] buffer = new byte[length];
			System.arraycopy(this._received_buffer, 0, buffer, 0, length);
			return buffer;
		}

		return null;
	}

	public String receiveString() throws IOException {
		
		DatagramPacket receivePacket = new DatagramPacket(this._received_buffer, this._received_buffer.length);
		this._socket.receive(receivePacket);
		int length = receivePacket.getLength();
		if (length > 0) {
			byte[] buffer = new byte[length];
			System.arraycopy(this._received_buffer, 0, buffer, 0, length);
			return new String(buffer);
		}

		return null;
	}

	public String getAddress() { return this._address; }

	public int getPort() { return this._port; }

	public int getTimeout() { return this._timeout; }

	public byte[] getReceivedBuffer() { return this._received_buffer; }

	
	// ----------------- Attributes -------------------
	
	private byte[] _received_buffer = new byte[UDP.BufferSize];

	private DatagramSocket _socket = null;
	private InetAddress _inet_address = null;

	private String _address = "";
	private int _port = -1;
	private int _timeout = UDP.Timeout;
}
