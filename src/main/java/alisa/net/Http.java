package alisa.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class Http {
		
	// request
	public Response request(String url, String body, String method, int timeout) {
		
		byte[] b = body.getBytes(StandardCharsets.UTF_8);
		return this._request(url, b, method, timeout);
	}
	public Response request(String url, byte[] body, String method, int timeout) {
		
		return this._request(url, body, method, timeout);
	}
	public Response request(String url, String method, int timeout) {
		
		return this._request(url, null, method, timeout);
	}
	
	// post
	public Response post(String url, String body, int timeout) {
		
		return this.request(url, body, "POST", timeout);
	}
	public Response post(String url, byte[] body, int timeout) {
		
		return this._request(url, body, "POST", timeout);
	}	
	
	// get
	public Response get(String url, int timeout) {
		
		return this._request(url, null, "GET", timeout);
	}	

	
	private Response _request(String url, byte[] body, String method, int timeout) {
		
		Response response = null;
		
		try {
			URL url_conn = new URL(url);			
			this._conn = (HttpURLConnection) url_conn.openConnection();
			
			// timeout
			if (timeout > 0) { this._conn.setReadTimeout(timeout); }
			
			// method
			this._conn.setRequestMethod(method);

			// header
			for (int i = 0; i < this._headers.size(); i++) {
				Header header = this._headers.get(i);
				this._conn.setRequestProperty(header.key, header.value);
			}
						
			// request (with body)
			if (body != null) {
				this._conn.setDoOutput(true);
				this._out = this._conn.getOutputStream();
				this._out.write(body);
				this._out.flush();
			}
		
			// response
			this._in = new BufferedReader(new InputStreamReader(this._conn.getInputStream(), StandardCharsets.UTF_8));
			StringBuilder buffer = new StringBuilder();
			String line;
			while ((line = this._in.readLine()) != null) { buffer.append(line); }
			
			if (buffer.length() > 0) {
				response = new Response(this._conn.getResponseCode(), buffer.toString());	
			}
			else {
				response = new Response(this._conn.getResponseCode(), "");
			}
		} 
		catch (IOException ex) { 
			this._error = ex.getMessage(); 
			
			try { response = new Response(this._conn.getResponseCode(), ""); } 
			catch (IOException ex1) { }
		} 
		finally {
			if (this._out != null) try { this._out.close(); this._out = null; }  catch (IOException ex) { }			
			if (this._in != null) try { this._in.close(); this._in = null; } catch (IOException ex) { }
			if (this._conn != null) { this._conn.disconnect(); this._conn = null; }
		}
		
		return response;
	}
	
	
	// -------------- Attributes ---------------
	
	HttpURLConnection _conn = null;
	OutputStream _out = null;
	BufferedReader _in = null;
	
	public void newHeaders() { this._headers = new ArrayList<>(); }
	public void addHeader(String key, String value) { this._headers.add(new Header(key, value)); }
	private ArrayList<Header> _headers = new ArrayList<>();
	
	public String error() { return this._error; }
	private String _error = "";
}
