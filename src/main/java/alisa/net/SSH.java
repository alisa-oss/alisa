package alisa.net;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.ByteArrayOutputStream;
import org.json.JSONObject;

public class SSH {
	
	public SSH() {
		
	}
	public SSH(String username, String password, String hostname, int tunnel_port) {
		
		this._username = username;
		this._password = password;
		this._hostname = hostname;
		this._port = tunnel_port;	
	}
	
	public boolean load(JSONObject root_json) {
		
		// username
		try { this._username = root_json.getString("username"); }
		catch (Exception ex) { this._error = "username"; return false; }
		
		// password
		try { this._password = root_json.getString("password"); }
		catch (Exception ex) { this._error = "password"; return false; }
		
		// hostname
		try { this._hostname = root_json.getString("hostname"); }
		catch (Exception ex) { this._error = "hostname"; return false; }
		
		// port
		try { this._port = root_json.getInt("port"); }
		catch (Exception ex) { this._error = "port"; return false; }
		
//		// local_port
//		try { this._local_port = root_json.getInt("local_port"); }
//		catch (Exception ex) { this._error = "local_port"; return false; }
//		
//		// remote_port
//		try { this._remote_port = root_json.getInt("remote_port"); }
//		catch (Exception ex) { this._error = "remote_port"; return false; }
		
		// timeout - option
		try { this._timeout = root_json.getInt("timeout"); }
		catch (Exception ex) { }
		
		return true;
	}
		
	public boolean connect() {
	
		try {
			this._session = new JSch().getSession(this._username, this._hostname, this._port);
			if (this._timeout > 0) { this._session.setTimeout(this._timeout); }
			this._session.setPassword(this._password);
			this._session.setConfig("StrictHostKeyChecking", "no");
			this._session.connect(); 	
			
			return true;
		}
		catch (Exception ex) { this._error = ex.getMessage(); }

		return false;
	}
	
	public boolean createTunnel(int local_port, int remote_port) {
	
		try {
			this._session.setPortForwardingL(local_port, "localhost", remote_port);
			return true;
		}
		catch (Exception ex) { this._error = ex.getMessage(); }
		
		return false;
	}	
	
	public boolean closeTunnel(int local_port) {
		
		try {
			this._session.delPortForwardingL(local_port);
			return true;
		}
		catch (JSchException ex) { }
		
		return false;
	}
	
	public String execute(String command) {
	
		try {
			ChannelExec channel = (ChannelExec) this._session.openChannel("exec");
			channel.setCommand(command);
			ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
			channel.setOutputStream(responseStream);
			channel.connect();

			while (channel.isConnected()) {
				Thread.sleep(100);
			}		

			return new String(responseStream.toByteArray());
		}
		catch (InterruptedException ex1) { Thread.currentThread().interrupt(); }
		catch (Exception ex2) {  }

		return "";
	}
	
	public void close() {
		
		try {			
			this._session.disconnect();
			this._session = null;
		}
		catch (Exception ex) { }
	}
	
	
	// ---------------------- Attributes ----------------------
	
	public String error() { return this._error; }
	private String _error = "";
	
	private String _username = "";
	private String _password = "";
	private String _hostname = "";
	private Integer _port = -1;
	private Integer _timeout = -1;
	
	public Session session() { return this._session; }
	private Session _session = null;
}
