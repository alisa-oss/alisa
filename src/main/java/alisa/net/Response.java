package alisa.net;

public class Response {

	public Response(int code, String body) {

		this.code = code;
		this.body = body;
	}

	public int code = -1;
	public String body = "";
}
