package alisa.regression;

public class Model {
		
	public boolean result = false;

	public int N = 0; // number of data
	public int p = 0; // number of independent variables

	// y = a + b[0]x[0] + b[1]x[1] + ...
	public double a = 0.0;
	public double[] b = null;

	public double R2 = 0.0;
	public double R2_adj = 0.0;

	public double SST = 0.0;
	public double SSE = 0.0;
	public double SSR = 0.0;
}
