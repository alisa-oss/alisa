package alisa.regression;

public class LinearRegression {

	public static Model calculate(double[] x, double[] y) {
		
		// sum
		int N = x.length;
		int p = 1;

		double sum_x = 0;
		double sum_y = 0;
		double sum_xy = 0;
		double sum_x2 = 0;
		for (int i = 0; i < N; i++) {
			sum_x += x[i];
			sum_y += y[i];
			sum_xy += x[i] * y[i];
			sum_x2 += Math.pow(x[i], 2);
		}

		// x_bar, y_bar
		double x_bar = sum_x / N;
		double y_bar = sum_y / N;

		// model
		Model model = new Model();
		model.N = N;
		model.p = p;
		model.b = new double[1];
		double dividend= ((sum_x * sum_y) - (N * sum_xy));
		double divisor = ((sum_x * sum_x) - (N * sum_x2));
		if (divisor < 0.0 || divisor > 0.0) {
			model.b[0] = dividend / divisor;
			model.a = y_bar - (model.b[0] * x_bar);
		}
		else { return model; } // error (divided by 0)

		// y_hat
		double[] y_hat = new double[N];
		for (int i = 0; i < N; i++) {
			y_hat[i] = model.a;
			y_hat[i] += model.b[0] * x[i];
		}

		// SST, SSE, SSR
		double SST = 0.0;
		double SSE = 0.0;
		for (int i = 0; i < N; i++) {
			SST += Math.pow(y[i] - y_bar, 2);
			SSE += Math.pow(y[i] - y_hat[i], 2);
		}
		model.SST = SST;
		model.SSE = SSE;
		model.SSR = SST - SSE;

		// R2, adjusted R2
		if (SST > 0.0 || SST < 0.0) {
			model.R2 = 1 - (SSE / SST);
			model.R2_adj = 1 - ((1 - model.R2) * (model.N - 1) / (model.N - model.p - 1));
		}
		else { return model; } // error (divided by 0)

		model.result = true;
		return model;
	}
}
