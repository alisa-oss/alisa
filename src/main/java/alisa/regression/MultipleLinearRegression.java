package alisa.regression;

import Jama.Matrix;
import Jama.QRDecomposition;

public class MultipleLinearRegression {
	// y = a + b[0]x[0] + b[1]x[1] + ...

	public static Model calculate(double[][] x, double[] y) {
		
		if (x.length != y.length) {
			return null;
		}

		int N = y.length;       // number of data
		int p = x[0].length;    // number of dependent variables

		Matrix Y = new Matrix(y, N);

		// convert from x [x11, x12]  to  mx [1, x11, x12]
		//                [x21, x22]         [1, x21, x22]
		double[][] mx = new double[x.length][x[0].length + 1];
		for (int i = 0; i < mx.length; i++) {
			mx[i][0] = 1;
			for (int j = 0; j < x[0].length; j++) {
				mx[i][1 + j] = x[i][j];
			}
		}
		Matrix X = new Matrix(mx);

		// find least squares solution
		QRDecomposition qr = new QRDecomposition(X);
		Matrix beta = qr.solve(Y);

		// model
		Model model = new Model();
		model.N = N;
		model.p = p;
		model.a = beta.get(0, 0);
		model.b = new double[p];
		for (int i = 0; i < model.b.length; i++) {
			model.b[i] = beta.get(i + 1, 0);
		}

		// y_bar
		double y_bar = 0.0;
		for (int i = 0; i < N; i++) {
			y_bar += y[i];
		}
		y_bar /= N;

		// y_hat
		double[] y_hat = new double[N];
		for (int i = 0; i < N; i++) {
			y_hat[i] = model.a;
			for (int j = 0; j < p; j++) {
				y_hat[i] += model.b[j] * x[i][j];
			}
		}

		// SST, SSE, SSR
		double SST = 0.0;
		double SSE = 0.0;
		for (int i = 0; i < N; i++) {
			SST += Math.pow(y[i] - y_bar, 2);
			SSE += Math.pow(y[i] - y_hat[i], 2);
		}
		model.SST = SST;
		model.SSE = SSE;
		model.SSR = SST - SSE;

		// R2, adjusted R2
		if (SST < 0.0 || SST > 0.0) { model.R2 = 1 - (SSE / SST); }
		else { return model; } // error (divided by 0)
		
		double dividend=(1 - model.R2) * (model.N - 1);
		double divisor = (model.N - model.p - 1);
		if (divisor < 0.0 || divisor > 0.0) {
			model.R2_adj = 1 - (dividend / divisor);
		}
		else { return model; } // error (divided by 0)

		model.result = true;
		return model;
	}
}
