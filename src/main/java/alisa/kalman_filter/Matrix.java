package alisa.kalman_filter;

public class Matrix 
{
    protected int m_c = 0;
    protected int m_r = 0;

    public Matrix()
    {
        Data = null;
    }

    public Matrix(int cols, int rows)
    {
        Resize(cols, rows);
    }

    public Matrix(Matrix m)
    {
        Set(m);
    }

    public void Set(Matrix m)
    {
        Resize(m.getColumns(), m.getRows());
        for (int i = 0; i < m.Data.length; i++)
        {
            Data[i] = m.Data[i];
        }
    }

    public int getColumns() { return m_c; }
    public void setColumns(int value) { Resize(value, m_r); }

    public int getRows() { return m_r; }
    public void setRows(int value) { Resize(m_c, value); }

    public void Resize(int cols, int rows)
    {
        if ((m_c == cols) && (m_r == rows)) return;
        m_c = cols;
        m_r = rows;
        Data = new double[cols * rows];
        Zero();
    }

    public Matrix Clone()
    {
        Matrix m = new Matrix();
        m.Resize(this.getColumns(), this.getRows());
        for (int i = 0; i < Data.length; i++)
        {
            m.Data[i] = Data[i];
        }
        return m;
    }

    public double Get(int x, int y)
    {
        return Data[x + y * m_c];
    }

    public double Trace(int index)
    {
        return Get(index, index);
    }

    public void Set(int x, int y, double v)
    {
        Data[x + (y * m_c)] = v;
    }


    public double[] Data = null;

    public void Multiply(double scalar)
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] *= scalar;
        }
    }

    public static Matrix Multiply(Matrix m, double scalar)
    {
        Matrix rv = m.Clone();
        rv.Multiply(scalar);
        return rv;
    }

    public static Matrix Multiply(Matrix a, Matrix b)
    {
        Matrix rv = new Matrix(b.getColumns(), a.getRows());
        int min = a.getColumns() < b.getRows() ? a.getColumns() : b.getRows();
        for (int i = 0; i < a.getRows(); i++)
        {
            for (int j = 0; j < b.getColumns(); j++)
            {
                double s = 0;
                for (int k = 0; k < min; k++)
                {
                    double av = a.Get(k, i);
                    double bv = b.Get(j, k);
                    s += av * bv;
                }
                rv.Set(j, i, s);
            }
        }
        return rv;
    }

    public void Multiply(Matrix b)
    {
        Matrix tmp = Matrix.Multiply(this, b);
        this.Set(tmp);
    }

    public static Matrix MultiplyABAT(Matrix a, Matrix b)
    {
        Matrix rv = Multiply(a, b);
        Matrix t = Matrix.Transpose(a);
        rv.Multiply(t);
        return rv;
    }

    public static Matrix Add(Matrix a, double scalar)
    {
        Matrix rv = new Matrix(a);
        rv.Add(scalar);
        return rv;
    }

    public void Add(double scalar)
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] += scalar;
        }
    }

    public static Matrix Add(Matrix a, Matrix b)
    {
        Matrix rv = new Matrix(a);
        rv.Add(b);
        return rv;
    }

    public void Add(Matrix a)
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] += a.Data[i];
        }
    }

    public static Matrix Subtract(Matrix a, double scalar)
    {
        Matrix rv = new Matrix(a);
        rv.Subtract(scalar);
        return rv;
    }

    public void Subtract(double scalar)
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] -= scalar;
        }
    }

    public static Matrix Subtract(Matrix a, Matrix b)
    {
        Matrix rv = new Matrix(a);
        rv.Subtract(b);
        return rv;
    }

    public void Subtract(Matrix a)
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] -= a.Data[i];
        }
    }

    public static Matrix Transpose(Matrix m)
    {
        Matrix rv = new Matrix(m.m_r, m.m_c);
        for (int i = 0; i < m.m_c; i++)
        {
            for (int j = 0; j < m.m_r; j++)
            {
                rv.Set(j, i, m.Get(i, j));
            }
        }
        return rv;
    }

    public void Transpose()
    {
        Matrix rv = new Matrix(this.m_r, this.m_c);
        for (int i = 0; i < m_c; i++)
        {
            for (int j = 0; j < m_r; j++)
            {
                rv.Set(j, i, this.Get(i, j));
            }
        }
        this.Set(rv);
    }

    public boolean IsIdentity()
    {
        if (m_c != m_r) return false;
        int check = m_c + 1;
        int j = 0;
        for (int i = 0; i < Data.length; i++)
        {
            if (j == check)
            {
                j = 0;
                if (Data[i] != 1) return false;
            }
            else
            {
                if (Data[i] != 0) return false;
            }
            j++;
        }
        return true;
    }

    public void SetIdentity()
    {
        if (m_c != m_r) return;
        int check = m_c + 1;
        int j = 0;
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] = (j == check) ? 1 : 0;
            j = j == check ? 1 : j + 1;
        }
    }

    public void Zero()
    {
        for (int i = 0; i < Data.length; i++)
        {
            Data[i] = 0;
        }
    }

    public double getDeterminant()
    {
        if (m_c != m_r) return 0;

        if (m_c == 0) return 0;
        if (m_c == 1) return Data[0];
        if (m_c == 2) return (Data[0] * Data[3]) - (Data[1] * Data[2]);
        if (m_c == 3) return
            (Data[0] * ((Data[8] * Data[4]) - (Data[7] * Data[5]))) -
            (Data[3] * ((Data[8] * Data[1]) - (Data[7] * Data[2]))) +
            (Data[6] * ((Data[5] * Data[1]) - (Data[4] * Data[2])));

        // only supporting 1x1, 2x2 and 3x3
        return 0;
    }

    public static Matrix Invert(Matrix m)
    {
        if (m.m_c != m.m_r) return null;
        double det = m.getDeterminant();
        if (det == 0) return null;

        Matrix rv = new Matrix(m);
        if (m.m_c == 1) rv.Data[0] = 1 / rv.Data[0];
        det = 1 / det;
        if (m.m_c == 2)
        {
            rv.Data[0] = det * m.Data[3];
            rv.Data[3] = det * m.Data[0];
            rv.Data[1] = -det * m.Data[2];
            rv.Data[2] = -det * m.Data[1];
        }
        if (m.m_c == 3)
        {
            rv.Data[0] = det * (m.Data[8] * m.Data[4]) - (m.Data[7] * m.Data[5]);
            rv.Data[1] = -det * (m.Data[8] * m.Data[1]) - (m.Data[7] * m.Data[2]);
            rv.Data[2] = det * (m.Data[5] * m.Data[1]) - (m.Data[4] * m.Data[2]);

            rv.Data[3] = -det * (m.Data[8] * m.Data[3]) - (m.Data[6] * m.Data[5]);
            rv.Data[4] = det * (m.Data[8] * m.Data[0]) - (m.Data[6] * m.Data[2]);
            rv.Data[5] = -det * (m.Data[5] * m.Data[0]) - (m.Data[3] * m.Data[2]);

            rv.Data[6] = det * (m.Data[7] * m.Data[3]) - (m.Data[6] * m.Data[4]);
            rv.Data[7] = -det * (m.Data[7] * m.Data[0]) - (m.Data[6] * m.Data[2]);
            rv.Data[8] = det * (m.Data[4] * m.Data[0]) - (m.Data[3] * m.Data[1]);
        }
        return rv;
    }
}
