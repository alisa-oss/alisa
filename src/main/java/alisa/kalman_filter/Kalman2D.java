package alisa.kalman_filter;

public class Kalman2D 
{
    Matrix m_x = new Matrix(1, 2);
    Matrix m_p = new Matrix(2, 2);
    Matrix m_q = new Matrix(2, 2);

    double m_r;

    public double getValue() { return m_x.Data[0]; }
    public void setValue(double value) { m_x.Data[0] = value; }

    public double getVelocity() { return m_x.Data[1]; }

    public double getLastGain() { return m_last_gain; }
    protected double m_last_gain = 0.0;

    public double getVariance() { return m_p.Data[0]; }   

    public double Predicition(double dt)
    {
        return m_x.Data[0] + (dt * m_x.Data[1]);
    }

    public double Variance(double dt)
    {
        return m_p.Data[0] + dt * (m_p.Data[2] + m_p.Data[1]) + dt * dt * m_p.Data[3] + m_q.Data[0];
    }

    public void Reset(double qx, double qv, double r, double pd, double ix)    
    {
        m_q.Data[0] = qx * qx;
        m_q.Data[1] = qv * qx;
        m_q.Data[2] = qv * qx;
        m_q.Data[3] = qv * qv;

        m_r = r;
        m_p.Data[0] = m_p.Data[3] = pd;
        m_p.Data[1] = m_p.Data[2] = 0;
        m_x.Data[0] = ix;
        m_x.Data[1] = 0;
    }

    public double Update(double mx, double mv, double dt)
    {
        Matrix f = new Matrix(2, 2); f.Data = new double [] { 1, dt, 0, 1 };            
        Matrix h = new Matrix(2, 2); h.Data = new double[] { 1, 0, 0, 1 };
        Matrix ht = new Matrix(2, 2); ht.Data = new double[] { 1, 0, 0, 1 };

        m_x = Matrix.Multiply(f, m_x);

        m_p = Matrix.MultiplyABAT(f, m_p);
        m_p.Add(m_q);

        Matrix y = new Matrix(1, 2); y.Data = new double[] { mx - m_x.Data[0], mv - m_x.Data[1] };

        Matrix s = Matrix.MultiplyABAT(h, m_p);
        s.Data[0] += m_r;
        s.Data[3] += m_r*0.1;

        Matrix tmp = Matrix.Multiply(m_p, ht);
        Matrix sinv = Matrix.Invert(s);
        Matrix k = new Matrix(2, 2); // inited to zero.

        if (sinv != null)
        {
            k = Matrix.Multiply(tmp, sinv);              
        }

        m_last_gain = k.getDeterminant();


        m_x.Add(Matrix.Multiply(k, y));

        Matrix kh = Matrix.Multiply(k, h);
        Matrix id = new Matrix(2, 2); id.Data = new double[] { 1, 0, 0, 1 };
        kh.Multiply(-1);
        id.Add(kh);
        id.Multiply(m_p);
        m_p.Set(id);

        return m_x.Data[0];
    }
}
