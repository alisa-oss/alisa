package alisa.kalman_filter;

public class Kalman1D 
{
        protected double [] m_x = new double[2];
        protected double [] m_p = new double[4];
        protected double [] m_q = new double[4];
        protected double m_r;

        protected double m_last_gain = 0.0;

        public double getValue() { return m_x[0]; }
        public void setValue(double value) { m_x[0] = value; }
        
        public double getVelocity() { return m_x[1]; }

        public double getLastGain() { return m_last_gain; }
        
        public double getVariance() { return m_p[0]; }   

        public double Predicition(double dt)
        {
            return m_x[0] + (dt * m_x[1]);
        }

        public double Variance(double dt)
        {
            return m_p[0] + dt * (m_p[2] + m_p[1]) + dt * dt * m_p[3] + m_q[0];
        }

        public void Reset(double qx, double qv, double r, double pd, double ix)    
        {
            m_q[0] = qx; m_q[1] = qv;
            m_r = r;
            m_p[0] = m_p[3] = pd; 
            m_p[1] = m_p[2] = 0;
            m_x[0] = ix; 
            m_x[1] = 0;
        }

        public double Update(double m, double dt)
        {
            double oldX = m_x[0];
            m_x[0] = m_x[0] + (dt * m_x[1]);

            m_p[0] = m_p[0] + dt * (m_p[2] + m_p[1]) + dt * dt * m_p[3] + m_q[0];
            m_p[1] = m_p[1] + dt * m_p[3] + m_q[1];
            m_p[2] = m_p[2] + dt * m_p[3] + m_q[2];
            m_p[3] = m_p[3] + m_q[3];

            double y0 = m - m_x[0];
            double y1 = ((m - oldX) / dt) - m_x[1];

            double s = m_p[0] + m_r;

            double k = m_p[0] / s;
            m_last_gain = k;

            m_x[0] += y0 * k;
            m_x[1] += y1 * k;

            for (int i = 0; i < 4; i++) m_p[i] = m_p[i] - k * m_p[i];

            return m_x[0];
        }
}
