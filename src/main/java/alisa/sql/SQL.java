package alisa.sql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SQL 
{
    public boolean connect(String connection_strings) 
    {                
        try 
        {            
            this._connection = DriverManager.getConnection(connection_strings); 
        } 
        catch (SQLException e) 
        {            
            this._error = e.getMessage(); 
            return false; 
        }
        
        return true;
    }
    
    public void close() 
    {        
        // statement
        for (Statement statement : this._statement) 
        {            
            if (statement != null) try { statement.close(); } catch (SQLException ex) { }
        }
        
        // result_set
        for (ResultSet result_set : this._result_set) 
        {            
            if (result_set != null) try { result_set.close(); } catch (SQLException ex) { }
        }
        
        // connection
        if (this._connection != null) { try { this._connection.close(); } catch (SQLException ex) { } }
    }

	public String getDriverName() {
		
		if (this._connection != null) {
			try {
				DatabaseMetaData meta = this._connection.getMetaData();
				return meta.getDriverName();
			}
			catch (SQLException ex) {
			}
		}

		return "";
	}

	public String getDriverVersion() {
		
		if (this._connection != null) {
			try {
				DatabaseMetaData meta = this._connection.getMetaData();
				return meta.getDriverVersion();
			}
			catch (SQLException ex) { }
		}

		return "";
	}

	// execute
	public boolean execute(String sql) {

		Statement statement = null;
		boolean result = false;
		try {
			statement = this._connection.createStatement();
			statement.execute(sql);
			result = true;
		}
		catch (SQLException e) {
			this._error = e.getMessage();
		}

		if (statement != null) {
			this._statement.add(statement);
		}
		return result;
	}

	public int executeInsert(String sql) {

		Statement statement = null;
		ResultSet rs = null;
		int result = -1;
		try {
			statement = this._connection.createStatement();
			statement.execute(sql, Statement.RETURN_GENERATED_KEYS);

			rs = statement.getGeneratedKeys();

			if (rs.next()) {
				result = rs.getInt(1);
			}
		}
		catch (SQLException ex) {
			this._error = ex.getMessage();
		}

		if (statement != null) {
			this._statement.add(statement);
		}
		if (rs != null) {
			this._result_set.add(rs);
		}

		return result;
	}

	public int executeUpdate(String sql) {

		Statement statement = null;
		int result = -1;
		try {
			statement = this._connection.createStatement();
			result = statement.executeUpdate(sql);
		}
		catch (SQLException e) {
			this._error = e.getMessage();
		}

		if (statement != null) {
			this._statement.add(statement);
		}
		return result;
	}

	public ResultSet executeQuery(String sql) {
		
		Statement statement = null;
		ResultSet rs = null;
		try {
			statement = this._connection.createStatement();
			rs = statement.executeQuery(sql);
		}
		catch (SQLException e) {
			this._error = e.getMessage();
		}

		if (statement != null) {
			this._statement.add(statement);
		}
		if (rs != null) {
			this._result_set.add(rs);
		}
		return rs;
	}

	public int executeCount(String sql) {
		
		Statement statement = null;
		ResultSet rs = null;
		int result = -1;
		try {
			statement = this._connection.createStatement();
			rs = statement.executeQuery(sql);
			if (rs.next()) {
				result = rs.getInt(1);
			}
		}
		catch (SQLException e) {
			this._error = e.getMessage();
		}

		if (statement != null) {
			this._statement.add(statement);
		}
		if (rs != null) {
			this._result_set.add(rs);
		}
		return result;
	}

    // select
    public ResultSet select(String select, String from) 
    {        
        this._sql = "SELECT " + select + " FROM " + from;
        return this.executeQuery(this._sql);
    }

    public ResultSet select(String select, String from, String where) 
    {        
        this._sql = "SELECT " + select + " FROM " + from + " WHERE " + where;
        return this.executeQuery(this._sql);
    }
    
    public ResultSet select(String select, String from, String where, String order) 
    {        
        this._sql = "SELECT " + select + " FROM " + from;
        if (where.length() > 0)  this._sql += " WHERE " + where;
        if (order.length() > 0)  this._sql += " ORDER BY " + order;
        return this.executeQuery(this._sql);
    }

    // count
    public int count(String table) 
    {        
        this._sql = "SELECT COUNT(*) FROM " + table;
        return this.executeCount(this._sql);
    }    
    public int count(String table, String where) 
    {        
        this._sql = "SELECT COUNT(*) FROM " + table + " WHERE " + where;
        return this.executeCount(this._sql);
    }

    // insert
    public boolean insert(String table, String[] values) 
    {        
        this._sql = "INSERT INTO " + table + " VALUES (";
        for (int i = 0; i < values.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += "'" + values[i].replaceAll("'", "''") + "'";
        }
        this._sql += ")";
        
        return this.execute(this._sql);
    }

    public boolean insert(String table, String[] names, String[] values) 
    {        
        if (names.length != values.length) { return false; }

        this._sql = "INSERT INTO " + table + " (";
        for (int i = 0; i < names.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += names[i];
        }
        this._sql += ") VALUES (";
        for (int i = 0; i < values.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += "'" + values[i].replaceAll("'", "''") + "'";
        }
        this._sql += ")";

        return this.execute(this._sql);
    }

    public int insertAndGetID(String table, String[] values) 
    {        
        this._sql = "INSERT INTO " + table + " VALUES (";
        for (int i = 0; i < values.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += "'" + values[i].replaceAll("'", "''") + "'";
        }
        this._sql += ")";

        return this.executeInsert(this._sql);
    }

    public int insertAndGetID(String table, String[] names, String[] values) 
    {        
        if (names.length != values.length) { return -1; }

        this._sql = "INSERT INTO " + table + " (";
        for (int i = 0; i < names.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += names[i];
        }
        this._sql += ") VALUES (";
        for (int i = 0; i < values.length; i++) 
        {
            if (i > 0) { this._sql += ","; }
            this._sql += "'" + values[i].replaceAll("'", "''") + "'";
        }
        this._sql += ")";

        return this.executeInsert(this._sql);
    }

    public int update(String table, String where, String[] names, String[] values) {
        if (names.length != values.length) { return -1; }

        this._sql = "UPDATE " + table + " SET ";//name = ? , "
        for (int i = 0; i < names.length; i++) {
            if (i > 0) { this._sql += ", "; }
            this._sql += names[i] + " = '" + values[i].replaceAll("'", "''") + "'";
        }
        this._sql += " WHERE " + where;

        return this.executeUpdate(this._sql);
    }

    // delete 
    public int delete(String table, String where) {
        this._sql = "DELETE FROM " + table + " WHERE " + where;

        return this.executeUpdate(this._sql);
    }

    public boolean isTable(String table) {
        
        try {
            
            DatabaseMetaData meta = this._connection.getMetaData();
            ResultSet tables = meta.getTables(null, null, "My_Table_Name", null);
            if (tables.next()) {
                
                return tables.getString("TABLE_NAME").equals(table);
            }
        } catch (SQLException ex) { }

        return false;
    }

    // ----------- Attributes ------------
    public String getError() { return this._error; }
    protected String _error = "";

    public boolean isConnected() { return (this._connection != null); }
    protected Connection _connection = null;
    
    public String getLastSQL() { return this._sql; }
    protected String _sql = "";
    
    ArrayList<Statement> _statement = new ArrayList<>();
    ArrayList<ResultSet> _result_set = new ArrayList<>();
}
