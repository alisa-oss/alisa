package alisa.sql;

import java.sql.DriverManager;

// Required: mysql-connector-java-8.0.13.jar
public class MySQL extends SQL {

	public boolean connect(String address, String database, String user, String password) {
		
		//this._url = "jdbc:mysql://" + address + "/" + database + "?useSSL=true&requireSSL=false";
		this._address = address;
		this._database = database;
		this._user = user;
		this._password = password;

		return this.reconnect();
	}

	public boolean connect(String address, String database, String user, String password, String timezone) {
		
//		this._url = "jdbc:mysql://" + address + "/" + database + "?"
//				  + "serverTimezone=" + timezone + "&useSSL=true&requireSSL=false";
		this._timezone = timezone;
		this._address = address;
		this._database = database;
		this._user = user;
		this._password = password;

		return this.reconnect();
	}

	public boolean reconnect() {

		try {
			//Class.forName("com.mysql.jdbc.Driver");
			
			String url = "jdbc:mysql://" + this._address + "/" + this._database + "?" 
					  + "useSSL=true&requireSSL=false";
			if (this._timezone.length() > 0) url += "&serverTimezone=" + this._timezone;
			this._connection = DriverManager.getConnection(url, this._user, this._password);
		} catch (Exception e) {
			this._error = e.getMessage();
			return false;
		}

		return true;
	}

	
	// ------------------ Attributes ---------------------
	
	private String _timezone = "";
	private String _address = "";
	private String _database = "";	
	private String _user = "";
	private String _password = "";
}
