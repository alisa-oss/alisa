package alisa.sql;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

// Required: sqlite.jdbc-3.23.1.jar

public class SQLite extends SQL
{            
    @Override
    public boolean connect(String file_path)
    {        
        this._url = "jdbc:sqlite:" + file_path;
        
        return this.reconnect();
    }
    
    public boolean reconnect() {
        
        try {             
            this._connection = DriverManager.getConnection(this._url); 
        } 
        catch (SQLException e) { 
            
            this._error = e.getMessage(); 
            return false; 
        }        
        
        return true;
    }
    
    @Override
    public int insertAndGetID(String table, String[] values) 
    {
        if (!this.insert(table, values)) return -1;
        
        ResultSet rs = this.executeQuery("SELECT last_insert_rowid()");
        try {

            rs.next();
            return rs.getInt(1);
        }
        catch (SQLException ex) {} 
        
        return -1;
    }
    
    @Override
    public int insertAndGetID(String table, String[] names, String[] values) 
    {
        if (!this.insert(table, names, values)) return -1;
        
        ResultSet rs = this.executeQuery("SELECT last_insert_rowid()");
        try 
        {
            rs.next();
            return rs.getInt(1);
        }
        catch (SQLException ex) {} 
        
        return -1;
    }
    
    
    // ------------------ Attributes ---------------------
    
    private String _url = "";
}
