package alisa.sql;

import java.sql.DriverManager;

// Required: 8.4.1.jre11
public class SQLServer extends SQL {

	public boolean connect(String address, String database, String user, String password) {
		
		this._address = address;
		this._database = database;
		this._user = user;
		this._password = password;

		return this.reconnect();
	}

	public boolean reconnect() {

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			
			String url = "jdbc:sqlserver://" + this._address + ";database=" + this._database;
			this._connection = DriverManager.getConnection(url, this._user, this._password);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			this._error = e.getMessage();
			return false;
		}

		return true;
	}

	
	// ------------------ Attributes ---------------------
	
	private String _address = "";
	private String _database = "";
	private String _user = "";
	private String _password = "";
}
