package alisa.sql;

import java.util.Properties;
// Required: cache-jdbc-2.0.0.jar

public class Cache extends SQL
{
    public boolean connect(String address, int port, String namespace, String user, String password)
    {
        this._address = address;
        this._port = port;
        this._namespace = namespace;
        this._username = user;
        this._password = password;
                
        return this.reconnect();
    }
    
    public boolean reconnect() {
        
        try {            
//            Class.forName ("com.intersys.jdbc.CacheDriver").newInstance(); // deprecated
            Class.forName ("com.intersys.jdbc.CacheDriver").getDeclaredConstructor().newInstance();
            String  url="jdbc:Cache://" + this._address + ":" + this._port + "/" + this._namespace;
            java.sql.Driver drv = java.sql.DriverManager.getDriver(url);

            java.util.Properties props = new Properties();
            props.put("user", this._username);
            props.put("password", this._password);
            this._connection = drv.connect(url, props);
        }
        catch (Exception ex) { this._error = ex.getMessage();  return false; }
        
        return true;
    }
    
    // ------------------ Attributes ---------------------
    
    private String _url = "";    
    
    private String _address = "";
    private int _port = -1;
    private String _namespace = "";
    private String _username = "";
    private String _password = "";
}
