package alisa.ga;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

public class GA {

	public GA() {
		
		m_elitism = false;
		m_mutationRate = 0.05;
		m_crossoverRate = 0.80;
		m_populationSize = 100;
		m_generationSize = 2000;
	}

	public GA(double crossoverRate, double mutationRate, int populationSize, int generationSize, int genomeSize) {
		
		m_elitism = false;
		m_mutationRate = mutationRate;
		m_crossoverRate = crossoverRate;
		m_populationSize = populationSize;
		m_generationSize = generationSize;
		m_genomeSize = genomeSize;
	}

	public GA(int genomeSize) {
		
		m_elitism = false;
		m_genomeSize = genomeSize;
	}

	public void Go() {
		
		if (getFitness == null) {
			this._error = "Need to supply fitness function";
			return;
		}
		if (m_genomeSize == 0) {
			this._error = "Genome size not set";
			return;
		}

		//  Create the fitness table.
		m_fitnessTable = new ArrayList<>();
		m_thisGeneration = new ArrayList<>(m_generationSize);
		m_nextGeneration = new ArrayList<>(m_generationSize);
		Genome.setMutationRate(m_mutationRate);

		CreateGenomes();
		RankPopulation();

		for (int i = 0; i < m_generationSize; i++) {
			CreateNextGeneration();
			RankPopulation();
		}
	}

	private int RouletteSelection() {
		
		double randomFitness = m_random.nextDouble() * m_totalFitness;
		int idx = -1;
		int mid;
		int first = 0;
		int last = m_populationSize - 1;
		mid = (last - first) / 2;

		while (idx == -1 && first <= last) {
			if (randomFitness < (double) m_fitnessTable.get(mid)) { last = mid; }
			else if (randomFitness > (double) m_fitnessTable.get(mid)) { first = mid; }
			
			mid = (first + last) / 2;
			//  lies between i and i+1
			if ((last - first) == 1) { idx = last; }
		}
		return idx;
	}

	private void RankPopulation() {
		
		m_totalFitness = 0;
		for (int i = 0; i < m_populationSize; i++) {
			Genome g = m_thisGeneration.get(i);
			double fitness = getFitnessFunction().cal(g.Genes());// FitnessFunction(g.Genes());
			g.setFitness(fitness);
			m_totalFitness += g.getFitness();
		}
		Collections.sort(m_thisGeneration);

		//  now sorted in order of fitness.
		double fitness = 0.0;
		m_fitnessTable.clear();
		for (int i = 0; i < m_populationSize; i++) {
			fitness += m_thisGeneration.get(i).getFitness();
			m_fitnessTable.add(fitness);
		}
	}

	private void CreateGenomes() {
		
		for (int i = 0; i < m_populationSize; i++) {
			Genome g = new Genome(m_genomeSize);
			m_thisGeneration.add(g);
		}
	}

	private void CreateNextGeneration() {
		
		m_nextGeneration.clear();
		Genome g = null;
		if (m_elitism) { g = m_thisGeneration.get(m_populationSize - 1); }

		for (int i = 0; i < m_populationSize; i += 2) {
			int pidx1 = RouletteSelection();
			int pidx2 = RouletteSelection();
			Genome parent1 = m_thisGeneration.get(pidx1);

			Generation gen = new Generation();
			gen.parent = m_thisGeneration.get(pidx2);

			if (m_random.nextDouble() < m_crossoverRate) { parent1.Crossover(gen); }
			else {
				gen.child1 = parent1;
				gen.child2 = gen.parent;
			}

			gen.child1.Mutate();
			gen.child2.Mutate();

			m_nextGeneration.add(gen.child1);
			m_nextGeneration.add(gen.child2);
		}
		
		if (m_elitism && g != null) { m_nextGeneration.set(0, g); }

		m_thisGeneration.clear();
		for (int i = 0; i < m_populationSize; i++) {
			m_thisGeneration.add(m_nextGeneration.get(i));
		}
	}

	public GAFunction getFitnessFunction() { return getFitness; }

	public void setFitnessFunction(GAFunction value) { getFitness = value; }

	public int getPopulationSize() { return m_populationSize; }

	public void setPopulationSize(int value) { m_populationSize = value; }

	public int getGenerations() { return m_generationSize; }

	public void setGeneration(int value) { m_generationSize = value; }

	public int getGenomeSize() { return m_genomeSize; }

	public void setGenomeSize(int value) { m_genomeSize = value; }

	public double getCrossoverRate() { return m_crossoverRate; }

	public void setCrossoverRate(double value) { m_crossoverRate = value; }

	public double getMutationRate() { return m_mutationRate; }

	public void setMutationRate(double value) { m_mutationRate = value; }

	public boolean getElitism() { return m_elitism; }

	public void setElitism(boolean value) { m_elitism = value; }

	public void GetBest(Generation gen) {
		
		Genome g = m_thisGeneration.get(m_populationSize - 1);
		gen.values = new double[g.getLength()];
		g.GetValues(gen);
		gen.fitness = g.getFitness();
	}

	public void GetWorst(Generation gen) { GetNthGenome(0, gen); }

	public void GetNthGenome(int n, Generation gen) {
		
		if (n < 0 || n > m_populationSize - 1) { return; }
		Genome g = m_thisGeneration.get(n);
		gen.values = new double[g.getLength()];
		g.GetValues(gen);
		gen.fitness = g.getFitness();
	}

	private double m_mutationRate;
	private double m_crossoverRate;
	private int m_populationSize;
	private int m_generationSize;
	private int m_genomeSize;
	private double m_totalFitness;
	private boolean m_elitism;

	private ArrayList<Genome> m_thisGeneration;
	private ArrayList<Genome> m_nextGeneration;
	private ArrayList<Double> m_fitnessTable;

	private final static SecureRandom m_random = new SecureRandom();

	static private GAFunction getFitness;

	public String getError() { return this._error; }
	private String _error = "";
}
