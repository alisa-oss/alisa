package alisa.ga;

import java.security.SecureRandom;

public class Genome implements Comparable<Genome> {

	public Genome() { }
	public Genome(int length) {
		
		m_length = length;
		m_genes = new double[length];
		CreateGenes();
	}
	public Genome(int length, boolean createGenes) {
		
		m_length = length;
		m_genes = new double[length];
		if (createGenes) { CreateGenes(); }
	}

	private void CreateGenes() {
		
		for (int i = 0; i < m_length; i++) {
			m_genes[i] = m_random.nextDouble();
		}
	}

	public void Crossover(Generation gen) {
		
		int pos = (int) (m_random.nextDouble() * (double) m_length);
		gen.child1 = new Genome(m_length, false);
		gen.child2 = new Genome(m_length, false);
		for (int i = 0; i < m_length; i++) {
			if (i < pos) {
				gen.child1.m_genes[i] = m_genes[i];
				gen.child2.m_genes[i] = gen.parent.m_genes[i];
			}
			else {
				gen.child1.m_genes[i] = gen.parent.m_genes[i];
				gen.child2.m_genes[i] = m_genes[i];
			}
		}
	}

	public void Mutate() {
		
		for (int pos = 0; pos < m_length; pos++) {
			if (m_random.nextDouble() < m_mutationRate) {
				m_genes[pos] = (m_genes[pos] + m_random.nextDouble()) / 2.0;
			}
		}
	}

	public double[] Genes() { return m_genes; }

	public void GetValues(Generation gen) {
		
		for (int i = 0; i < m_length; i++) { gen.values[i] = m_genes[i]; }
	}

	public void setFitness(double value) { m_fitness = value; }

	public double getFitness() { return m_fitness; }

	public static void setMutationRate(double value) { m_mutationRate = value; }

	public static double getMutationRate() { return m_mutationRate; }

	public int getLength() { return m_length; }

	@Override
	public int compareTo(Genome y) {
		
		if (this.getFitness() > y.getFitness()) { return 1; }
		else if (this.getFitness() == y.getFitness()) { return 0; }
		else { return -1; }
	}
	
	private double[] m_genes;
	private int m_length;
	private double m_fitness;
	//private static Random m_random = new Random();
	private final static SecureRandom m_random = new SecureRandom();

	private static double m_mutationRate;

}
