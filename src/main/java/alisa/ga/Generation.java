package alisa.ga;

public class Generation 
{
    public Genome parent = null;
    public Genome child1 = null;
    public Genome child2 = null;
    
    public double[] values = null;
    public double fitness = 0;
}
