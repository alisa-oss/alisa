package alisa.ga;

public interface GAFunction 
{
    double cal(double[] values);
}
