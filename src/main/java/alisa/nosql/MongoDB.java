package alisa.nosql;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import org.bson.Document;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoIterable;
import java.sql.Timestamp;
import org.json.JSONException;

public class MongoDB {

	// disable log
	static Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
	static { root.setLevel(Level.OFF); }
	
	public MongoDB() { }
	public MongoDB(String connection_string, String database) {
		
		this._connection_string = connection_string;
		this._database_name = database;
	}
	
	public boolean load(JSONObject root_json) {
		
		// connection_string
		try { this._connection_string = root_json.getString("connection_string"); }
		catch (JSONException ex) {
			
			try { 
				String hostname = root_json.getString("hostname"); 
				String username = root_json.getString("username"); 
				String password = root_json.getString("password"); 
				int port = root_json.getInt("port"); 
				
				this._connection_string = "mongodb://" + username + ":" + password + "@" 
						  + hostname + ":" + port;
			}
			catch (JSONException ex2) { this._error = "connection_string"; return false; }
		}
		
		// database
		try { this._database_name = root_json.getString("database"); }
		catch (JSONException ex) { this._error = "database"; return false; }
		
		return true;
	}
	
	public String getBuildInfo() {
		
		MongoDatabase db = this._mongo_client.getDatabase("admin");
		Document document = db.runCommand(new Document("buildInfo", 1));
		return document.toJson();
	}
	
	public MongoCollection getCollection(String collection) {
		
		try { 
			return this._database.getCollection(collection);
		}
		catch (Exception ex) { this._error = ex.getMessage(); }
		return null;
	}
	
	public boolean connect() {
		
		try {
			this._mongo_client = MongoClients.create(this._connection_string);			
			this._database = this._mongo_client.getDatabase(this._database_name);			
			return true;
		}
		catch (Exception ex) {
			this._error = ex.getMessage();
		}
		
		if (this._mongo_client != null) { 
			this._mongo_client.close(); 
			this._mongo_client = null; 
		}

		return false;
	}

	public void close() {

		if (this._mongo_client != null) {
			this._mongo_client.close(); this._mongo_client = null;
		}
	}
	
	public Document findOne(String collection, String filter, String sort) {
				
		try {
			MongoCollection mongo_collection = this._database.getCollection(collection);
			MongoIterable<Document> iterable = mongo_collection.find(Document.parse(filter))
				  .sort(Document.parse(sort));
			return iterable.first();
		}
		catch (Exception ex) {
			this._error =  ex.getMessage();
		}

		return null;
	}
	public Document findOne(String collection, String filter, String sort, String projection) {
				
		try {
			MongoCollection mongo_collection = this._database.getCollection(collection);
			MongoIterable<Document> iterable = mongo_collection.find(Document.parse(filter))
				  .sort(Document.parse(sort))
				  .projection(Document.parse(projection));
			return iterable.first();
		}
		catch (Exception ex) { this._error =  ex.getMessage(); }

		return null;
	}
	
	public boolean insert(String collection, String json) {

		try {
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.insertOne(Document.parse(json));

			return true;
		}
		catch (Exception ex) { this._error = ex.getMessage(); }

		return false;
	}
	
	public boolean update(String collection, String condition, String key, String  json) {

		try {
			Document doc = new Document(key, Document.parse(json));
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.updateOne(Document.parse(condition), new Document("$set", doc));

			return true;
		}
		catch (Exception ex) { }

		return false;
	}
	
	public boolean updateString(String collection, String condition, String key, String value) {

		try {
			Document doc = new Document(key, value);
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.updateOne(Document.parse(condition), doc);

			return true;
		}
		catch (Exception ex) { }

		return false;
	}
	
	public boolean updateInt(String collection, String condition, String key, int value) {

		try {
			Document doc = new Document(key, value);
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.updateOne(Document.parse(condition), doc);

			return true;
		}
		catch (Exception ex) { }

		return false;
	}
	public boolean updateDouble(String collection, String condition, String key, double value) {

		try {
			Document doc = new Document(key, value);
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.updateOne(Document.parse(condition), doc);

			return true;
		}
		catch (Exception ex) { }

		return false;
	}
	
	
	public boolean updateDateTime(String collection, String condition, String key, alisa.DateTime datetime) {
				
		try {			
			Timestamp timestamp = new Timestamp(datetime.getDate().getTime());
			Document doc = new Document(key, timestamp);			
			MongoCollection mongo_collection = this._database.getCollection(collection);
			mongo_collection.updateOne(Document.parse(condition), new Document("$set", doc));
			
			return true;
		}
		catch (Exception ex) { }

		return false;
	}

	public Document findOne(String database, String collection, 
			  String filter, String sort, String projection,
			  int skip) {
				
		try {
			MongoDatabase mongo_database = this._mongo_client.getDatabase(database);
			MongoCollection mongo_collection = mongo_database.getCollection(collection);			
			
			FindIterable iterable = null;			
			if (filter.length() > 0)  iterable = mongo_collection.find(Document.parse(filter));
			else iterable = mongo_collection.find();

			if (sort.length() > 0)  iterable = iterable.sort(Document.parse(sort));
			
			if (projection.length() > 0)  iterable = iterable.projection(Document.parse(projection));
			
			if (skip > 0) iterable = iterable.skip(skip);
						
			return (Document)iterable.first();
		}
		catch (Exception ex) { this._error =  ex.getMessage(); }

		return null;
	}
	
	public ArrayList<Document> find(String database, String collection, 
			  String filter, String sort, String projection,
			  int skip, int limit) {
				
		try {
			MongoDatabase mongo_database = this._mongo_client.getDatabase(database);
			MongoCollection mongo_collection = mongo_database.getCollection(collection);			
			
			FindIterable iterable = null;
			
			if (filter.length() > 0)  iterable = mongo_collection.find(Document.parse(filter));
			else iterable = mongo_collection.find();

			if (sort.length() > 0)  iterable = iterable.sort(Document.parse(sort));
			
			if (projection.length() > 0)  iterable = iterable.projection(Document.parse(projection));
			
			if (skip > 0) iterable = iterable.skip(skip);
			
			if (limit > 0) iterable = iterable.limit(limit);
			
			ArrayList<Document> docs = new ArrayList<>();
			iterable.into(docs);
			return docs;
			//return iterable.iterator();
		}
		catch (Exception ex) {
			this._error =  ex.getMessage();
		}

		return null;
	}	
	
	
	// ----------------- Attributes -------------------
	
	public MongoClient mongo_client() { return this._mongo_client; }
	private MongoClient _mongo_client = null;	
	
	public MongoDatabase database() { return this._database; }
	private MongoDatabase _database = null;
	
	public String connection_string() { return this._connection_string; }
	private String _connection_string = "";	
	
	private String _database_name = "";

	public String error() { return this._error; }
	private String _error = "";
}
