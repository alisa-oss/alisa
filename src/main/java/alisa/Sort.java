package alisa;

public class Sort {

    public static void quicksort(int[] items)
    {
        Sort._qs(items, 0, items.length - 1);
    }
    public static void quicksort(double[] items)
    {
        Sort._qs(items, 0, items.length - 1);
    }

    public static void quicksort(int[] items, int[] index)
    {
        Sort._qs(items, index, 0, items.length - 1);
    }
    public static void quicksort(double[] items, int[] index)
    {
        Sort._qs(items, index, 0, items.length - 1);
    }

    private static void _qs(int[] items, int left, int right)
    {
        int i, j, x, y;
        i = left; j = right;
        x = items[(left + right) / 2];
        do
        {
            while ((items[i] < x) && (i < right)) i++;
            while ((x < items[j]) && (j > left)) j--;

            if (i <= j)
            {
                y = items[i]; items[i] = items[j]; items[j] = y;
                i++; j--;
            }
        } while (i <= j);
        if (left < j) _qs(items, left, j);
        if (i < right) _qs(items, i, right);
    }
    private static void _qs(int[] items, int[] index, int left, int right)
    {
        int i, j, x, y;
        i = left; j = right;
        x = items[(left + right) / 2];

        do
        {
            while ((items[i] < x) && (i < right)) i++;
            while ((x < items[j]) && (j > left)) j--;

            if (i <= j)
            {
                y = items[i]; items[i] = items[j]; items[j] = y;
                y = index[i]; index[i] = index[j]; index[j] = y;
                i++; j--;
            }
        }
        while (i <= j);

        if (left < j) _qs(items, index, left, j);
        if (i < right) _qs(items, index, i, right);
    }
    
    private static void _qs(double[] items, int left, int right)
    {
        int i = left; 
        int j = right;
        double x = items[(left + right) / 2];
        double temp;
        do
        {
            while ((items[i] < x) && (i < right)) i++;
            while ((x < items[j]) && (j > left)) j--;

            if (i <= j)
            {
                temp = items[i]; items[i] = items[j]; items[j] = temp;
                i++; j--;
            }
        } while (i <= j);
        if (left < j) _qs(items, left, j);
        if (i < right) _qs(items, i, right);
    }
    private static void _qs(double[] items, int[] index, int left, int right)
    {
        int temp_i;
        double temp_d;
        int i = left; 
        int j = right;
        double x = items[(left + right) / 2];

        do
        {
            while ((items[i] < x) && (i < right)) i++;
            while ((x < items[j]) && (j > left)) j--;

            if (i <= j)
            {
                temp_d = items[i]; items[i] = items[j]; items[j] = temp_d;
                temp_i = index[i]; index[i] = index[j]; index[j] = temp_i;
                i++; j--;
            }
        }
        while (i <= j);

        if (left < j) _qs(items, index, left, j);
        if (i < right) _qs(items, index, i, right);
    }
}
