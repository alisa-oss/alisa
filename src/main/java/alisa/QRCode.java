package alisa;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import javax.imageio.ImageIO;

public class QRCode {

	public static boolean encode(String text, String image_path, int image_size) {

		int index = image_path.lastIndexOf(".");
		String image_type = image_path.substring(index + 1);
		//System.out.println(image_type);

		BufferedImage image = encode(text, image_size);
		if (image == null) {
			return false;
		}

		File file = new File(image_path);
		try {
			ImageIO.write(image, image_type, file);
			return true;
		} catch (IOException ex) {
		}

		return false;
	}

	public static BufferedImage encode(String text, int image_size) {

		try {
			Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
			hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

			hintMap.put(EncodeHintType.MARGIN, 1);
			/* default = 4 */
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix byteMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE,
					  image_size, image_size, hintMap);
			int CrunchifyWidth = byteMatrix.getWidth();
			BufferedImage image = new BufferedImage(CrunchifyWidth,
					  CrunchifyWidth,
					  BufferedImage.TYPE_INT_RGB);
			image.createGraphics();

			Graphics2D graphics = (Graphics2D) image.getGraphics();
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
			graphics.setColor(Color.BLACK);

			for (int i = 0; i < CrunchifyWidth; i++) {
				for (int j = 0; j < CrunchifyWidth; j++) {
					if (byteMatrix.get(i, j)) {
						graphics.fillRect(i, j, 1, 1);
					}
				}
			}

			return image;
		} catch (WriterException e) {
		}

		return null;
	}

	public static String decode(BufferedImage image) {

		String decoded = "";

		try {
			BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(new BufferedImageLuminanceSource(image)));

			Reader reader = new QRCodeReader();
			Result result = reader.decode(bitmap);
			decoded = result.getText();
		} catch (ChecksumException | FormatException | NotFoundException ex) {
		}

		return decoded;
	}

	public static String decode(String image_path) {

		File file = new File(image_path);
		BufferedImage image = null;
		try {
			image = ImageIO.read(file);
		} catch (IOException ex) {
			return "";
		}

		return decode(image);
	}

}
