package alisa;

import java.security.SecureRandom;
import java.util.Arrays;

public class Calculator {

	public double median(double[] numArray) {
		
		Arrays.sort(numArray);
		double median;
		if (numArray.length % 2 == 0) {
			median = (numArray[numArray.length / 2] + numArray[numArray.length / 2 - 1]) / 2;
		} else {
			median = numArray[numArray.length / 2];
		}

		return median;
	}

	public int median(int[] numArray) {
		
		Arrays.sort(numArray);
		int median;
		if (numArray.length % 2 == 0) {
			median = (numArray[numArray.length / 2] + numArray[numArray.length / 2 - 1]) / 2;
		} else {
			median = numArray[numArray.length / 2];
		}

		return median;
	}

	public static int random(int max) {
		
		return (int) Math.round((max * random.nextDouble()));
	}

	public static int random(int min, int max) {
		
		return min + (int) Math.round(((max - min) * random.nextDouble()));
	}
	
	public static int signedToUnsigned(int signed, int bit) {
		
		int max = (1 << bit);		
		return (signed & (max-1));
	}
	
	public static int unsignedToSigned(int unsigned, int bit) {
		
		int max = (1 << bit);
		int half = max / 2;				
		unsigned = (unsigned & (max-1));
		if (unsigned >= half) { return unsigned - max;} 
		// else
		return unsigned;
	}
	
	private final static SecureRandom random = new SecureRandom(); 
}
