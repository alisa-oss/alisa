package alisa;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Zip {

	public boolean compressFile(String file_path, String zip_file_path) {
		
		boolean result = true;
		
		File f = new File(file_path);

		byte[] buffer = new byte[1024];
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		FileInputStream fis = null;
		
		try {
			fos = new FileOutputStream(zip_file_path);
			zos = new ZipOutputStream(fos);
			fis = new FileInputStream(f);
			zos.putNextEntry(new ZipEntry(f.getName()));
			int length;
			while ((length = fis.read(buffer)) > 0) {
				zos.write(buffer, 0, length);
			}
		}
		catch (IOException ex) { 
			this._error = ex.getMessage(); 
			result = false; // failed
		}

		// close
		if (zos != null) { 
			try { zos.closeEntry(); } catch (IOException ex) { } 
			try { zos.close(); } catch (IOException ex) { } 
		}
		if (fis != null) { try { fis.close(); } catch (IOException ex) { } }
		if (fos != null) { try { fos.close(); } catch (IOException ex) { } }
		
		return result;
	}

	public boolean compressFiles(String[] file_paths, String zip_file_path) {
		
		boolean result = true;
		
		byte[] buffer = new byte[1024];
		FileOutputStream fos = null;
		ZipOutputStream zos = null;

		try {
			fos = new FileOutputStream(zip_file_path);
			zos = new ZipOutputStream(fos);
		}
		catch (IOException ex) { 
			this._error = ex.getMessage(); 
			result = false; // failed
		}
		
		if (result && fos != null && zos != null) {
			for (int i = 0; i < file_paths.length; i++) {
				FileInputStream fis = null;
				try {
					File f = new File(file_paths[i]);
					fis = new FileInputStream(f);

					zos.putNextEntry(new ZipEntry(f.getName()));
					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}
				}
				catch (IOException ex) { 
					this._error = ex.getMessage(); 
					result = false;  // failed
				}

				if (fis != null) { try { fis.close(); } catch (IOException ex) { } }
				try { zos.closeEntry(); } catch (IOException ex) { }
				
				if (!result) break;
			}
		}

		// close zos & fos
		if (zos != null) { try { zos.close(); } catch (IOException ex) { } }
		if (fos != null) { try { fos.close(); } catch (IOException ex) { } }

		return result;
	}

	public boolean compressFolder(String folder_path, String zip_file_path) {
		
		boolean result = true;
		
		// visit all files & folders
		this._file_paths = new ArrayList<>();
		this._file_names = new ArrayList<>();
		File f = new File(folder_path);
		this._visit(f, "");

		byte[] buffer = new byte[1024];
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		
		try {
			fos = new FileOutputStream(zip_file_path);
			zos = new ZipOutputStream(fos);
		}
		catch (IOException ex) {
			this._error = ex.getMessage();
			result = false; // failed
		}
		
		if (result && fos != null && zos != null) {
			for (int i = 0; i < this._file_paths.size(); i++) {
				FileInputStream fis = null;
				try {
					File srcFile = new File(this._file_paths.get(i));
					fis = new FileInputStream(srcFile);
					zos.putNextEntry(new ZipEntry(this._file_names.get(i)));
					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}
				}
				catch (IOException ex) { 
					this._error = ex.getMessage();
					result = false;
				}
				
				if (fis != null) { try { fis.close(); } catch (IOException ex) { } }
				try { zos.closeEntry(); } catch (IOException ex) { }
				
				if (!result) break;
			}
		}
		
		// close zos & fos
		if (zos != null) { try { zos.close(); } catch (IOException ex) { } }
		if (fos != null) { try { fos.close(); } catch (IOException ex) { } }		

		return result;
	}

	public boolean extract(String zip_file, String output_folder) {
		
		boolean result = true;
		
		ArrayList<ZipEntry> fileEntries = new ArrayList<>();
		ZipFile zip = null;
		try {
			zip = new ZipFile(zip_file);
			Enumeration zipEntries = zip.entries();
			
			while (zipEntries.hasMoreElements()) {
				ZipEntry zipEntry = (ZipEntry) zipEntries.nextElement();
				if (zipEntry.isDirectory()) {
					new File(output_folder, zipEntry.getName()).mkdirs();
				}
				else {
					fileEntries.add(zipEntry);
				}
			}			
		}
		catch (IOException ex) {
			this._error = ex.getMessage();
			result = false;
		}
		
		if (result && zip != null) {
			Iterator allFiles = fileEntries.iterator();
			while (allFiles.hasNext()) {
				FileOutputStream out = null;
				InputStream in = null;
				
				try {
					ZipEntry fileEntry = (ZipEntry) allFiles.next();
					File file = new File(output_folder, fileEntry.getName());
					if (file.setLastModified(fileEntry.getTime())) { }
					file.getParentFile().mkdirs();
					out = new FileOutputStream(file);
					in = zip.getInputStream(fileEntry);

					byte[] bufferserver = new byte[100000]; // 100kB
					while (true) {
						int size = in.read(bufferserver, 0, bufferserver.length);
						if (size <= 0) { break; }

						out.write(bufferserver, 0, size);
					}
				}
				catch (IOException ex) {
					this._error = ex.getMessage();
					result = false;
				}

				if (in != null) { try { in.close(); } catch (IOException ex) { } }
				if (out != null) { try { out.close(); } catch (IOException ex) { } }
				
				if (!result) break;
			}
		}

		if (zip != null) { try { zip.close(); } catch (IOException ex) { } }

		return result;
	}

	// --------------- internal methods ---------------
	
	private void _visit(File file, String p) {
		
		String path = file.getPath();
		String pathname = file.getName();

		p = p + pathname + "/";

		String[] filenames = file.list();
		for (int i = 0; i < filenames.length; i++) {
			File f = new File(path + "/" + filenames[i]);
			if (f.isFile()) {
				this._file_paths.add(f.getAbsolutePath());
				this._file_names.add(p + f.getName());
			}
			else if (f.isDirectory()) {
				this._visit(f, p);
			}
		}
	}

	
	// ------------- Attributes ------------------
	
	public String getError() { return this._error;}
	private String _error = "";

	private ArrayList<String> _file_paths = new ArrayList<>();
	private ArrayList<String> _file_names = new ArrayList<>();

}
