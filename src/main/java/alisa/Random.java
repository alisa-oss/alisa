package alisa;

import java.security.SecureRandom;

public class Random {

	public double random() {
		
		return this._random.nextDouble();
	}

	public int random(int max) {
		
		return (int) Math.round((max * this._random.nextDouble()));
	}

	public int random(int min, int max) {
		
		return min + (int) Math.round(((max - min) * this._random.nextDouble()));
	}

	
	// ------------------- Attributes ----------------------
	
	private final SecureRandom _random = new SecureRandom();
}
