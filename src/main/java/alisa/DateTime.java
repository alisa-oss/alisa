package alisa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTime 
{    
	// --------- Static Methods  ----------    
	
	public static int getOffset() {
		
		ZoneOffset offset = ZoneOffset.systemDefault().getRules().getOffset(Instant.now());
		return Integer.parseInt(offset.toString().substring(0, 3));
	}
	
	public static DateTime findPrevious(DateTime from, int year, int month, int day) {        
		
		Calendar c = Calendar.getInstance(Locale.ENGLISH);
		c.set(from.getYear() - year, 
				  from.getMonth() - month - 1, 
				  from.getDay() - day);
		return new DateTime(c.getTime());
	}    
	public static DateTime findPrevious(DateTime from, int day, int hour, int minute, int second) {
		
		long s = (day * 24 * 60 * 60) + (hour * 60 * 60) + (minute * 60) + (long)second;
		Date d = new Date(from.getTick() - (s * 1000));
		return new DateTime(d);
	}
    
	public static int countSecond(DateTime from, DateTime to) {
		
		return (int) Math.floor((to.getTick() - from.getTick()) / 1000.0);
	}
	
	public static double countPreciseSecond(DateTime from, DateTime to) {
		
		return (to.getTick() - from.getTick()) / 1000.0;
	}

	public static double countDay(DateTime from, DateTime to) {
		
		return (to.getTick() - from.getTick()) / 86400000.0;
	}

	public static DateTime findNext(DateTime from, int year, int month, int day) {        
		
		Calendar c = Calendar.getInstance(Locale.ENGLISH);
		c.set(from.getYear() + year, from.getMonth() + month, from.getDay() + day, 
			  from.getHour(), from.getMinute(), from.getSecond());
		return new DateTime(c.getTime());
	}  
	public static DateTime findNext(DateTime from, int day, int hour, int minute, int second) {
		
		int s = (day * 24 * 60 * 60) + (hour * 60 * 60) + (minute * 60) + second;
		Date d = new Date(from.getTick() + (s * 1000));
		return new DateTime(d);
	}
    
	// ISO8601
	//
	//  2020-02-03
	//  2020-02-03T04:40:55+00:00
	//  2020-02-03T04:40:55Z
	//  20200203T044055Z
	public static DateTime parseISO8601(String s) {
		try {
			DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
			OffsetDateTime offsetDateTime = OffsetDateTime.parse(s, timeFormatter);

			//offsetDateTime.getOffset();

			int year = offsetDateTime.getYear();
			int month = offsetDateTime.getMonthValue();
			int day = offsetDateTime.getDayOfMonth();
			int hour = offsetDateTime.getHour();
			int minute = offsetDateTime.getMinute();
			int second = offsetDateTime.getSecond();

			Calendar c = Calendar.getInstance(Locale.ENGLISH);
			c.set(year, month - 1, day, hour, minute, second);
			return new DateTime(c.getTime());
		}
		catch (Exception ex) {
		}

		return null;
	}

	// Parse string in 3 formats to DateTime with offset 
	//
	// 2020-02-03						// 1. only date
	// 2020-02-03T04:40:55Z			// 2. date-time
	// 2020-02-03T04:40:55.000Z		// 3. date-time with millisecond
	public static DateTime parseUTC(String s) {

		int offset = getOffset() * 60 * 60 * 1000; // hour x minute x second x millisecond
		
		switch (s.length()) {
			case 10: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
					Date d = ft.parse(s);
					return new DateTime(d.getTime() + offset);
				}
				catch (Exception ex) { }
			}
			break;
			case 20: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
					Date d = ft.parse(s);
					return new DateTime(d.getTime() + offset);
				}
				catch (Exception ex) { }
				
			}
			break;
			case 24: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
					Date d = ft.parse(s);
					return new DateTime(d.getTime() + offset);
				}
				catch (Exception ex) { }
				
			}
			break;
		}

		return null;
	}	

	// Parse string in 3 formats to DateTime
	//
	// 2020-02-03					// 1. only date
	// 2020-02-03 04:40:55			// 2. date-time
	// 2020-02-03 04:40:55.000		// 3. date-time with millisecond
	public static DateTime parse(String s) {

		switch (s.length()) {
			case 10: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
					Date d = ft.parse(s);
					return new DateTime(d.getTime());
				}
				catch (Exception ex) { }
			}
			break;
			case 19: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = ft.parse(s);
					return new DateTime(d.getTime());
				}
				catch (Exception ex) { }
				
			}
			break;
			case 23: {
				try {
					SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					Date d = ft.parse(s);
					return new DateTime(d.getTime());
				}
				catch (Exception ex) { }
				
			}
			break;
		}

		return null;
	}

	// --------- Constructors ----------
	
	public DateTime() { } // current time
	public DateTime(Date date) { 
		
		this._calendar.clear();
		this._calendar.setTime(date); 
	}
	public DateTime(long ms) { 
		
		this._calendar.clear();
		this._calendar.setTimeInMillis(ms); 
	}
	public DateTime(int Year, int Month, int Day) { 
		
		this._calendar.clear();
		this._calendar.set(Year, Month - 1, Day, 0,0,0); 
	}
	public DateTime(int Year, int Month, int Day, int Hour, int Minute, int Second) {
		
		this._calendar.clear();
		this._calendar.set(Year, Month - 1, Day, Hour, Minute, Second);
	}    	
	public DateTime(int Year, int Month, int Day, int Hour, int Minute, int Second, int ms) {
		
		this._calendar.clear();
		this._calendar.set(Year, Month - 1, Day, Hour, Minute, Second);
		this._calendar.setTimeInMillis(this._calendar.getTimeInMillis() + ms);
	}
    
	// --------- Methods ----------

	public int getYear() { return this._calendar.get(Calendar.YEAR); } 
	public int getMonth() { return this._calendar.get(Calendar.MONTH) + 1; } 
	public int getDay() { return this._calendar.get(Calendar.DAY_OF_MONTH); }
	public int getDay_of_week() { return this._calendar.get(Calendar.DAY_OF_WEEK); }
	public int getHour() { return this._calendar.get(Calendar.HOUR_OF_DAY); }
	public int getMinute() { return this._calendar.get(Calendar.MINUTE); }
	public int getSecond() { return this._calendar.get(Calendar.SECOND); }
	public int getMilliSecond() { return this._calendar.get(Calendar.MILLISECOND); }
 
	public void set(int Year, int Month, int Day, int Hour, int Minute, int Second) {
		
		this._calendar.set(Year, Month, Day, Hour, Minute, Second);
	}

	public void update() {
		
		this._calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.ENGLISH);
	}

	public Date getDate() { return this._calendar.getTime(); }

	public Calendar getCalendar() { return this._calendar; }

	public long getTick() { return this._calendar.getTime().getTime(); }

	public String formatDate() {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(this._calendar.getTime());
	}

	public String formatTime() {

		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		return df.format(this._calendar.getTime());
	}

	public String formatDateTime() {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.format(this._calendar.getTime());
	}

	public String formatUTCDateTime() {

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);
		return df.format(this._calendar.getTime());
	}
	
	public String toString() {
					
		return this.toDateString() + " " + this.toTimeString();
	}

	public String toPreciseString() {
				
		return this.toDateString() + " " + this.toPreciseTimeString();
	}
	
	public String toDateString() {

		// "YYYY-MM-dd"  
		String s = this.getYear() + "-";
		
		int month = this.getMonth();
		if (month < 10) { s += "0"; } 
		s += month + "-";
		
		int date = this.getDay();
		if (date < 10) { s += "0"; }
		s += date;

		return s;
	}

	public String toTimeString() {
		
		// "HH:mm:ss"  
		String s = "";
		
		int hour = this.getHour();
		if (hour < 10) { s += "0"; }
		s += hour + ":";
		
		int minute = this.getMinute();
		if (minute < 10) { s += "0"; }
		s += minute + ":";
		
		int second = this.getSecond();
		if (second < 10) { s += "0"; }
		s += second;
		
		return s;
	}
	
	public String toPreciseTimeString() {
		
		String s = this.toTimeString()+ ".";
		
		int ms = this.getMilliSecond();		
		if (ms < 10)  { s += "00"; }
		else if (ms < 100) { s += "0"; }
		s += ms;
		
		return s;
	}

	public void setTimeZone(TimeZone timezone) { this._calendar.setTimeZone(timezone); }	
	public TimeZone getTimeZone() { return this._calendar.getTimeZone(); }
    
     
    // --------- Attributes ----------
    
    private Calendar _calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.ENGLISH);
    
}