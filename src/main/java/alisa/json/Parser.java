package alisa.json;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Parser {

	public static boolean isObject(String json) { return (json.startsWith("{")); }

	public static boolean isArray(String json) { return (json.startsWith("[")); }

	public static JSONObject loadObject(String filepath) {

		try {
			String json = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
			return new JSONObject(json); 
		} 
		catch (IOException | JSONException ex) { }

		return null;
	}

	public static JSONArray loadArray(String filepath) {

		try {
			String json = new String(Files.readAllBytes(Paths.get(filepath)), StandardCharsets.UTF_8);
			return new JSONArray(json);
		}
		catch (IOException | JSONException ex) { }

		return null;
	}
}
