package alisa.fuzzy_logic;

public class Rule 
{
    public Rule(Linguistics linguistics, int subset_id)
    {
        this._input_linguistics = linguistics;
        this._output_subset_id = subset_id;
    }

    public float calculateMembership() { return this._input_linguistics.calculateMembership(); }
    public float getMembership() { return this._input_linguistics.getMembership(); }

    // input linguistics
    public Linguistics getInputLinguistics() { return this._input_linguistics; }
    private Linguistics _input_linguistics = null;
    
    // output subset id
    public void setOutputSubsetID(int value) { this._output_subset_id = value; }
    public int getOutputSubsetID() { return this._output_subset_id; }
    private int _output_subset_id = -1;    
}
