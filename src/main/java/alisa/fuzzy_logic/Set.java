package alisa.fuzzy_logic;

import java.util.ArrayList;

public class Set 
{
    public Set(String name, int id)
    {
        this._name = name;
        this._id = id;
    }

    public void fuzzificate(float input)
    {
        for (int i = 0; i < this.countSubsets(); i++) this.getSubset(i).fuzzificate(input);
    }

    public void clearAllMemberships() 
    {
        for (int i = 0; i < this.countSubsets(); i++) this.getSubset(i).clearMembership();
    }

    // id
    public int getID() { return this._id; }
    private int _id = -1;

    // name
    public String getName() { return this._name; }
    private String _name = "";

    // value
    public void setValue(float value)            
    {
        if (value < 0.0f) this._value = 0.0f; 
        else if (value > 1.0f) this._value = 1.0f; 
        else this._value = value;
    }
    public float getValue() { return this._value; }
    private float _value = 0.0f;

    // subsets
    public void addSubset(Subset subset) { this._subsets.add(subset); }
    public Subset getSubset(int index) { return this._subsets.get(index); }
    public int countSubsets() { return this._subsets.size(); }
    private ArrayList<Subset> _subsets = new ArrayList<Subset>();
}
