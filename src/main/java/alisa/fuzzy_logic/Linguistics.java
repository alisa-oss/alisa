package alisa.fuzzy_logic;

public class Linguistics 
{
    public static enum Operation
    {
        None, OR, AND, NOT
    }   
    
    public void setup(Operation operation) { this._operation = operation; }
    public void setup(Set set, int subset_id) { this._set = set; this._subset_id = subset_id; }

    public float calculateMembership()
    {
        if (this._operation == Operation.None) this._membership = this._set.getSubset(this._subset_id).getMembership();
        else if (this._operation == Operation.OR) this._membership = this.OR(this._first_child.calculateMembership(), this._second_child.calculateMembership());
        else if (this._operation == Operation.AND) this._membership = this.AND(this._first_child.calculateMembership(), this._second_child.calculateMembership());
        else if (this._operation == Operation.NOT) this._membership = this.NOT(this._first_child.calculateMembership());
        return this._membership;
    }

    public Operation getOperation() { return this._operation; }
    private Operation _operation = Operation.None;

    public void setFirstChild(Linguistics linguistics) { this._first_child = linguistics; }
    public Linguistics getFirstChild() { return this._first_child; }
    private Linguistics _first_child = null;

    public void setSecondChild(Linguistics linguistics) { this._second_child = linguistics; }
    public Linguistics getSecondChild() { return this._second_child; }
    private Linguistics _second_child = null;

    private float OR(float a, float b) { if (a > b) return a; else return b; /* max */ }
    private float AND(float a, float b) { if (a < b) return a; else return b; /* min */ }
    private float NOT(float a) { return (1.0f - a); /* invert */ }

    public Set getSet() { return this._set; }  
    private Set _set = null;

    public int getSubsetID() { return this._subset_id; }
    private int _subset_id = -1;

    public float getMembership() { return this._membership; }
    private float _membership = 0.0f;    
}
