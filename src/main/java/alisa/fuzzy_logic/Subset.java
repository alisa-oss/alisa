package alisa.fuzzy_logic;

public class Subset 
{
    public Subset(String name, float lb, float lt, float rt, float rb)
    {
        this._name = name;

        this.setLeftBottom(lb);
        this.setLeftTop(lt);
        this.setRightTop(rt);
        this.setRightBottom(rb);
    }

    // name
    public String getName() { return this._name; }
    private String _name = "";

    // membership
    public void setMembership(float value) 
    {
        if (value < 0.0f) this._membership = 0.0f; 
        else if (value > 1.0f) this._membership = 1.0f; 
        else this._membership = value;
    }
    public float getMembership() { return this._membership; }
    private float _membership = 0.0f;

    // centriod
    public float getCentroid() 
    {
        float half_trapezoid = (this._rt - this._lt + this._rb - this._lb)/4.0f;
        float triangle = (this._lt - this._lb)/2.0f;
        return this._lt + half_trapezoid - triangle;
    }

    // border
    
    public void setLeftBottom(float value)
    {
        if (value < 0.0f) this._lb = 0.0f; 
        else if (value > 1.0f) this._lb = 1.0f; 
        else this._lb = value;
    }
    public float getLeftBottom() { return this._lb; }
    private float _lb = 0.0f;


    public void setLeftTop(float value)
    { 
        if (value < 0.0f) this._lt = 0.0f; 
        else if (value > 1.0f) this._lt = 1.0f; 
        else this._lt = value; 
    }
    public float getLeftTop() { return this._lt; }
    private float _lt = 0.0f;

    public void setRightTop(float value)
    { 
        if (value < 0.0f) this._rt = 0.0f; 
        else if (value > 1.0f) this._rt = 1.0f; 
        else this._rt = value;
    }
    public float getRightTop() { return this._rt; }
    private float _rt = 0.0f;

    public void setRightBottom(float value)
    { 
        if (value < 0.0f) this._rb = 0.0f; 
        else if (value > 1.0f) this._rb = 1.0f; 
        else this._rb = value;
    }
    public float getRightBottom() { return this._rb; }
    private float _rb = 0.0f;

    public void clearMembership() { this._membership = 0.0f; }

    public void fuzzificate(float input)
    {
        if (input < this._lb || input > this._rb)
        {
            // out of Border
            this._membership = 0.0f;
        }
        else if (input >= this._lb && input < this._lt)
        {
            // [lb, lt)
            float w = this._lt - this._lb;
            if (w == 0) this._membership = 0.0f;
            else this._membership = (input - this._lb) / w;
        }
        else if (input >= this._lt && input <= this._rt)
        {
            // [lt, rt]
            this._membership = 1.0f;
        }

        // (Border2, Border3]
        else if (input > this._rt && input <= this._rb)
        {
            float w = this._rb - this._rt;
            if (w == 0) this._membership = 0.0f;
            else this._membership = 1.0f - ((input - this._rt) / w);
        }
    }
}
