package alisa.fuzzy_logic;

import java.util.ArrayList;

public class FuzzyLogic {
	// input sets

	private ArrayList<Set> _input_sets = new ArrayList<Set>();

	public Set getIntputSet(int index) {
		return this._input_sets.get(index);
	}

	public int countInputSets() {
		return this._input_sets.size();
	}

	// output set
	public Set getOutputSet() {
		return this._output_set;
	}
	private Set _output_set = null;

	// rules
	private ArrayList<Rule> _rules = new ArrayList<Rule>();

	public Rule getRule(int index) {
		return this._rules.get(index);
	}

	public int countRules() {
		return this._rules.size();
	}

	public boolean isDefaultSubsetID() {
		return this._is_default_subset_id;
	}
	private boolean _is_default_subset_id = false;

	public int getDefaultSubsetID() {
		return this._default_subset_id;
	}
	private int _default_subset_id = -1;

	public float getOutput() {
		return this._output;
	}
	private float _output = -1;

	private String _temp_string = "";

	public boolean infer() {
		// clear Output Membership
		this.getOutputSet().clearAllMemberships();

		// Fuzzification
		for (int i = 0; i < this.countInputSets(); i++) {
			Set input_set = this.getIntputSet(i);
			input_set.fuzzificate(input_set.getValue());
		}

		// Rule evaluation
		for (int i = 0; i < this.countRules(); i++) {
			Rule rule = this.getRule(i);
			rule.calculateMembership();

			// Output's Membership using Maximum Rule's Membership
			Subset subset = this.getOutputSet().getSubset(rule.getOutputSubsetID());
			if (rule.getMembership() > subset.getMembership()) {
				subset.setMembership(rule.getMembership());
			}
		}

		// Aggregation of rule consequents
		this._output = this.aggregate();

		// Defuzzification
		for (int i = 0; i < this._output_set.countSubsets(); i++) {
			this.getOutputSet().fuzzificate(this._output);
		}

		// set Output Value
		this._output_set.setValue(this._output);
		if (this._is_default_subset_id) {
			return false;
		}

		return true;
	}

	private float aggregate() {
		float x = 0, y = 0;

		for (int i = 0; i < this._output_set.countSubsets(); i++) {
			Subset subset = this._output_set.getSubset(i);
			x += subset.getMembership() * subset.getCentroid();
			y += subset.getMembership();
		}

		// if no any rules fire -> using DefaultSubsetID
		if (y == 0) {
			this._is_default_subset_id = true;
			return this.getOutputSet().getSubset(this._default_subset_id).getCentroid();
		}

		this._is_default_subset_id = false;
		return x / y;
	}

	private void _printLinguistics(Linguistics linguistics) {
		if (linguistics.getOperation() != Linguistics.Operation.None) {
			if (linguistics.getOperation() == Linguistics.Operation.NOT) {
				if (linguistics.getFirstChild().getOperation() == Linguistics.Operation.None) {
					this._temp_string += " NOT ";
					this._printLinguistics(linguistics.getFirstChild());
				}
				else {
					this._temp_string += " NOT (";
					this._printLinguistics(linguistics.getFirstChild());
					this._temp_string += ")";
				}
			}
			else if (linguistics.getOperation() == Linguistics.Operation.OR) {
				if (linguistics.getFirstChild().getOperation() == Linguistics.Operation.None) {
					this._printLinguistics(linguistics.getFirstChild());
				}
				else {
					this._temp_string += "(";
					this._printLinguistics(linguistics.getFirstChild());
					this._temp_string += ")";
				}

				this._temp_string += " OR ";

				if (linguistics.getSecondChild().getOperation() == Linguistics.Operation.None) {
					this._printLinguistics(linguistics.getSecondChild());
				}
				else {
					this._temp_string += "(";
					this._printLinguistics(linguistics.getSecondChild());
					this._temp_string += ")";
				}
				/*
                this._temp_string += "(";
                this._printLinguistics(linguistics.getFirstChild());
                this._temp_string += ") OR (";
                this._printLinguistics(linguistics.getSecondChild());
                this._temp_string += ")";
				 */
			}
			else if (linguistics.getOperation() == Linguistics.Operation.AND) {
				if (linguistics.getFirstChild().getOperation() == Linguistics.Operation.None) {
					this._printLinguistics(linguistics.getFirstChild());
				}
				else {
					this._temp_string += "(";
					this._printLinguistics(linguistics.getFirstChild());
					this._temp_string += ")";
				}

				this._temp_string += " AND ";

				if (linguistics.getSecondChild().getOperation() == Linguistics.Operation.None) {
					this._printLinguistics(linguistics.getSecondChild());
				}
				else {
					this._temp_string += "(";
					this._printLinguistics(linguistics.getSecondChild());
					this._temp_string += ")";
				}

				/*
                this._temp_string += "(";
                this._printLinguistics(linguistics.getFirstChild());
                this._temp_string += ") AND (";
                this._printLinguistics(linguistics.getSecondChild());
                this._temp_string += ")"; */
			}
		}
		else {
			Subset subset = linguistics.getSet().getSubset(linguistics.getSubsetID());
			this._temp_string += linguistics.getSet().getName() + " " + subset.getName();
		}
	}

	public String printIF(Rule rule) {
		this._temp_string = "";
		this._printLinguistics(rule.getInputLinguistics());
		return this._temp_string;
	}

	public String printTHEN(Rule rule) {
		Subset subset = this._output_set.getSubset(rule.getOutputSubsetID());
		return this._output_set.getName() + " " + subset.getName();
	}

	// Error
	public String getError() {
		return this._error;
	}
	private String _error = "";

	public boolean load(String pathname) {
		// Parsing
		alisa.xml.Parser xml = new alisa.xml.Parser();
		alisa.xml.Node fuzzylogic_node = xml.load(pathname);
		if (fuzzylogic_node == null) {
			this._error = "parsing failed";
			return false;
		}

		// <FuzzyLogic>
		if (!this._onFuzzyLogic(fuzzylogic_node)) {
			return false;
		}

		return true;
	}

	public boolean read(String s) {
		// Parsing
		alisa.xml.Parser xml = new alisa.xml.Parser();
		alisa.xml.Node fuzzylogic_node = xml.read(s);
		if (fuzzylogic_node == null) {
			this._error = "parsing failed";
			return false;
		}

		// <FuzzyLogic>
		if (!this._onFuzzyLogic(fuzzylogic_node)) {
			return false;
		}

		return true;
	}

	private boolean _onFuzzyLogic(alisa.xml.Node fuzzylogic_node) {
		if (!fuzzylogic_node.name.equals("FuzzyLogic")) {
			this._error = "<FuzzyLogic";
			return false;
		}

		// <Input>
		alisa.xml.Node input_node = fuzzylogic_node.findNode("Input");
		if (input_node == null) {
			this._error = "<FuzzyLogic><Input";
			return false;
		}
		if (!this._onInputNode(input_node)) {
			this._error = "<FuzzyLogic>" + this._error;
			return false;
		}

		// <Output>
		alisa.xml.Node output_node = fuzzylogic_node.findNode("Output");
		if (output_node == null) {
			this._error = "<FuzzyLogic><Output";
			return false;
		}
		if (!this._onOutputNode(output_node)) {
			this._error = "<FuzzyLogic>" + this._error;
			return false;
		}

		// <Rules>
		alisa.xml.Node rules_node = fuzzylogic_node.findNode("Rules");
		if (rules_node == null) {
			this._error = "<FuzzyLogic><Rules";
			return false;
		}
		if (!this._onRulesNode(rules_node)) {
			this._error = "<FuzzyLogic>" + this._error;
			return false;
		}

		return true;
	}

	private boolean _onInputNode(alisa.xml.Node input_node) {
		for (int set_id = 0; set_id < input_node.countNodes(); set_id++) {
			// <Set>
			alisa.xml.Node set_node = input_node.getNode(set_id);
			if (!set_node.name.equals("Set")) {
				this._error = "<Input><Set";
				return false;
			}

			String name = "";
			float lb = 0.0f;
			float lt = 0.0f;
			float rt = 0.0f;
			float rb = 0.0f;

			// id
			try {
				int id = Integer.parseInt(set_node.findAttribute("id").text);
				if (this.countInputSets() != id) {
					this._error = "<Input><Set id (not arranged)";
					return false;
				}
			}
			catch (Exception ex) {
				this._error = "<Input><Set id";
				return false;
			}

			// name
			try {
				name = set_node.findAttribute("name").text;
			}
			catch (Exception ex) {
				this._error = "<Input><Set id=\"" + set_id + "\" name";
				return false;
			}

			Set set = new Set(name, set_id);
			this._input_sets.add(set);

			for (int subset_id = 0; subset_id < set_node.countNodes(); subset_id++) {
				// <Subset>
				alisa.xml.Node subset_node = set_node.getNode(subset_id);
				if (!subset_node.name.equals("Subset")) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset";
					return false;
				}

				// id
				try {
					int id = Integer.parseInt(subset_node.findAttribute("id").text);
					if (set.countSubsets() != id || subset_id != id) {
						this._error = "<Input><Set id=\"" + set_id + "\"><Subset id (not arranged) ";
						return false;
					}
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset id";
					return false;
				}

				// name
				try {
					name = subset_node.findAttribute("name").text;
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset name";
					return false;
				}

				// lb
				try {
					lb = Float.parseFloat(subset_node.findAttribute("lb").text);
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset lb";
					return false;
				}

				// lt
				try {
					lt = Float.parseFloat(subset_node.findAttribute("lt").text);
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset lt";
					return false;
				}

				// rt
				try {
					rt = Float.parseFloat(subset_node.findAttribute("rt").text);
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset rt";
					return false;
				}

				// rb
				try {
					rb = Float.parseFloat(subset_node.findAttribute("rb").text);
				}
				catch (Exception ex) {
					this._error = "<Input><Set id=\"" + set_id + "\"><Subset rb";
					return false;
				}

				set.addSubset(new Subset(name, lb, lt, rt, rb));
			}
		}

		return true;
	}

	private boolean _onOutputNode(alisa.xml.Node output_node) {
		String name = "";
		int id = -1;
		float lb = 0.0f;
		float lt = 0.0f;
		float rt = 0.0f;
		float rb = 0.0f;

		try {
			name = output_node.findAttribute("name").text;
		}
		catch (Exception ex) {
			this._error = "<Output name";
			return false;
		}

		this._output_set = new Set(name, 0);

		for (int i = 0; i < output_node.countNodes(); i++) {
			// <Subset>
			alisa.xml.Node subset_node = output_node.getNode(i);
			if (!subset_node.name.equals("Subset")) {
				this._error = "<FuzzyLogic><Output><Subset";
				return false;
			}

			// id
			try {
				id = Integer.parseInt(subset_node.findAttribute("id").text);
			}
			catch (Exception ex) {
				this._error = "<Output><Subset id";
				return false;
			}
			if (id != this.getOutputSet().countSubsets()) {
				this._error = "<Output><Subset id (not arranged) ";
				return false;
			}

			// name
			try {
				name = subset_node.findAttribute("name").text;
			}
			catch (Exception ex) {
				this._error = "<Output><Subset name";
				return false;
			}

			// lb
			try {
				lb = Float.parseFloat(subset_node.findAttribute("lb").text);
			}
			catch (Exception ex) {
				this._error = "<Output><Subset lb";
				return false;
			}

			// lt
			try {
				lt = Float.parseFloat(subset_node.findAttribute("lt").text);
			}
			catch (Exception ex) {
				this._error = "<Output><Subset lt";
				return false;
			}

			// rt
			try {
				rt = Float.parseFloat(subset_node.findAttribute("rt").text);
			}
			catch (Exception ex) {
				this._error = "<Output><Subset rt";
				return false;
			}

			// rb
			try {
				rb = Float.parseFloat(subset_node.findAttribute("rb").text);
			}
			catch (Exception ex) {
				this._error = "<Output><Subset rb";
				return false;
			}

			this._output_set.addSubset(new Subset(name, lb, lt, rt, rb));
		}

		return true;
	}

	private boolean _onRulesNode(alisa.xml.Node rules_node) {
		// default_subset_id
		try {
			this._default_subset_id = Integer.parseInt(rules_node.findAttribute("default_subset_id").text);
		}
		catch (Exception ex) {
			this._error = "<Rules default_subset_id";
			return false;
		}

		for (int i = 0; i < rules_node.countNodes(); i++) {
			// <Rule>
			alisa.xml.Node rule_node = rules_node.getNode(i);
			if (!rule_node.name.equals("Rule")) {
				this._error = "<Rules><Rule";
				return false;
			}

			// id
			try {
				if (this.countRules() != Integer.parseInt(rule_node.findAttribute("id").text)) {
					this._error = "<FuzzyLogic><Rules><Rule id (not arranged)";
					return false;
				}
			}
			catch (Exception ex) {
				this._error = "<Rules><Rule id";
				return false;
			}

			try {
				// <IF>
				alisa.xml.Node if_node = rule_node.findNode("IF");

				Linguistics linguistics = new Linguistics();
				if (!this._onIF(if_node, linguistics)) {
					this._error = "<Rule id=\"" + i + "\">" + this._error;
					return false;
				}

				// <THEN>
				alisa.xml.Node then_node = rule_node.findNode("THEN");

				// subset_id
				int subset_id = -1;
				try { subset_id = Integer.parseInt(then_node.findAttribute("subset_id").text); }
				catch (Exception ex) {
					this._error = "<Rule id=\"" + i + "\"><THEN subset_id";
					return false;
				}

				// create & add rule
				Rule rule = new Rule(linguistics, subset_id);
				this._rules.add(rule);
			}
			catch (Exception ex) {
				this._error = "<Rules><Rule";
				return false;
			}
		}

		return true;
	}

	private boolean _onIF(alisa.xml.Node node, Linguistics linguistics) {
		for (int i = 0; i < node.countNodes(); i++) {
			alisa.xml.Node child_node = node.getNode(i);
			switch (child_node.name) {
				case "AND":
					// <AND>
					if (!this._onAND(child_node, linguistics)) {
						return false;
					}
					break;
				case "OR":
					// <OR>
					if (!this._onOR(child_node, linguistics)) {
						return false;
					}
					break;
				case "NOT":
					// <NOT>
					if (!this._onNOT(child_node, linguistics)) {
						return false;
					}
					break;
				case "Sentence":
					// <Sentence>
					if (!this._onSentence(child_node, linguistics)) {
						return false;
					}
					break;
			}
		}

		return true;
	}

	private boolean _onAND(alisa.xml.Node node, Linguistics linguistics) {
		int count = 0;
		linguistics.setup(Linguistics.Operation.AND);

		for (int i = 0; i < node.countNodes(); i++) {
			alisa.xml.Node child_node = node.getNode(i);
			switch (child_node.name) {
				case "AND": {
					// <AND>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onAND(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "OR": {
					// <OR>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onOR(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "NOT": {
					// <NOT>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onNOT(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "Sentence": {
					// <Sentence>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onSentence(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
			}
		}

		if (count != 2) {
			this._error = "<AND (not has 2 children)";
			return false;
		}

		return true;
	}

	private boolean _onOR(alisa.xml.Node node, Linguistics linguistics) {
		int count = 0;
		linguistics.setup(Linguistics.Operation.OR);

		for (int i = 0; i < node.countNodes(); i++) {
			alisa.xml.Node child_node = node.getNode(i);
			switch (child_node.name) {
				case "AND": {
					// <AND>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onAND(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "OR": {
					// <OR>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onOR(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "NOT": {
					// <NOT>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onNOT(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
				case "Sentence": {
					// <Sentence>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onSentence(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					else if (count == 1) {
						linguistics.setSecondChild(linguistics_child);
					}
					count++;
					break;
				}
			}
		}

		if (count != 2) {
			this._error = "<OR (not has 2 children)";
			return false;
		}

		return true;
	}

	private boolean _onNOT(alisa.xml.Node node, Linguistics linguistics) {
		int count = 0;
		linguistics.setup(Linguistics.Operation.NOT);

		for (int i = 0; i < node.countNodes(); i++) {
			alisa.xml.Node child_node = node.getNode(i);
			switch (child_node.name) {
				case "AND": {
					// <AND>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onAND(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					count++;
					break;
				}
				case "OR": {
					// <OR>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onOR(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					count++;
					break;
				}
				case "NOT": {
					// <NOT>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onNOT(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					count++;
					break;
				}
				case "Sentence": {
					// <Sentence>
					Linguistics linguistics_child = new Linguistics();
					if (!this._onSentence(child_node, linguistics_child)) {
						return false;
					}
					if (count == 0) {
						linguistics.setFirstChild(linguistics_child);
					}
					count++;
					break;
				}
			}
		}

		if (count != 1) {
			this._error = "<NOT (not has 1 child)";
			return false;
		}

		return true;
	}

	private boolean _onSentence(alisa.xml.Node node, Linguistics linguistics) {
		int set_id = -1;
		int subset_id = -1;

		// set_id
		try {
			set_id = Integer.parseInt(node.findAttribute("set_id").text);
		}
		catch (Exception ex) {
			this._error = "<Sentence set_id";
			return false;
		}

		// subset_id
		try {
			subset_id = Integer.parseInt(node.findAttribute("subset_id").text);
		}
		catch (Exception ex) {
			this._error = "<Sentence subset_id";
			return false;
		}

		Set set = this.getIntputSet(set_id);
		linguistics.setup(set, subset_id);

		return true;
	}
}
